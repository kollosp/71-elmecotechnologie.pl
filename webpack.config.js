const path = require('path');
const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackPartialsPlugin = require('html-webpack-partials-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyPlugin = require("copy-webpack-plugin");
const { VueLoaderPlugin } = require('vue-loader')

const loadImages = function(basicPath){
	const files = fs.readdirSync(basicPath)

	let ret = []

	files.forEach(value => {
		const p = path.join(basicPath, value)
		if(fs.lstatSync(p).isDirectory()){
			ret.push(...loadImages(p))
		}else {
			ret.push({
				from: p,
				to: "./" + value
			})
		}
	})

	return ret
}

module.exports = {
	entry: {
		index: './frontend/index.js',
		dashboard: './frontend/dashboard.js',
		blog: './frontend/blog.js',
		login: './frontend/login.js',
		error: './frontend/error.js',
		catalog: './frontend/catalog.js',
		product: './frontend/product.js',
		pvMain: './frontend/pvMain.js',
		contact: './frontend/contact.js',
		automationMain: './frontend/automationMain.js',
		electricServices: './frontend/electricServices.js',
		heating: './frontend/heating.js',
		powtranpolska: './frontend/powtranpolska.js',
	},
	output: {
		publicPath: '/',
		path: path.resolve(__dirname, 'public'),
		filename: '[name].js',
	},
	module: {
		rules: [{
			test: /\.(png|svg|jpg|gif)$/,
			use: [{
				loader: 'url-loader',
				options: {
					name: '[name].[ext]',
					outputPath: '/',
					esModule: false,
					// Images larger than 10 KB won’	t be inlined
					limit: 10 * 1024
				}
			}]
		},{
			test: /\.m?js$/,
			exclude: /(node_modules|bower_components)/,
			use: {
				loader: 'babel-loader',
				options: {
					presets: ['@babel/preset-env']
				}
			}
		},{
			test: /\.s[ac]ss$/i,
			use: [
				MiniCssExtractPlugin.loader,
				{
					loader: 'css-loader',
					options: { sourceMap: true }
				},
				'sass-loader',
			],
		},{
			test: /\.css$/,
			use: [
				'vue-style-loader',
				'css-loader'
			]
		},{
			test: /\.vue$/,
			loader: 'vue-loader',
		}],
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: "./frontend/views/index.html",
			filename: "index.ejs",
			//minify: true
			chunks: ['index']
		}),
		new HtmlWebpackPlugin({
			template: "./frontend/views/pvMain.html",
			filename: "pvMain.ejs",
			//minify: true
			chunks: ['pvMain']
		}),
		new HtmlWebpackPlugin({
			template: "./frontend/views/contact.html",
			filename: "contact.ejs",
			//minify: true
			chunks: ['contact']
		}),
		new HtmlWebpackPlugin({
			template: "./frontend/views/heating.html",
			filename: "heating.ejs",
			//minify: true
			chunks: ['heating']
		}),
		new HtmlWebpackPlugin({
			template: "./frontend/views/automationMain.html",
			filename: "automationMain.ejs",
			//minify: true
			chunks: ['automationMain']
		}),
		new HtmlWebpackPlugin({
			template: "./frontend/views/powtranpolska.html",
			filename: "powtranpolska.ejs",
			//minify: true
			chunks: ['powtranpolska']
		}),
		new HtmlWebpackPlugin({
			template: "./frontend/views/electricServices.html",
			filename: "electricServices.ejs",
			//minify: true
			chunks: ['electricServices']
		}),
		new HtmlWebpackPlugin({
			template: "./frontend/views/dashboard.html",
			filename: "dashboard.ejs",
			//minify: true
			chunks: ['dashboard']
		}),
		new HtmlWebpackPlugin({
			template: "./frontend/views/error.html",
			filename: "error.ejs",
			//minify: true
			chunks: ['error']
		}),
		new HtmlWebpackPlugin({
			template: "./frontend/views/blog.html",
			filename: "blog.ejs",
			//minify: true
			chunks: ['blog']
		}),
		new HtmlWebpackPlugin({
			template: "./frontend/views/catalog.html",
			filename: "catalog.ejs",
			//minify: true
			chunks: ['catalog']
		}),
		new HtmlWebpackPlugin({
			template: "./frontend/views/login.html",
			filename: "login.ejs",
			//minify: true
			chunks: ['login']
		}),
		new HtmlWebpackPlugin({
			template: "./frontend/views/product.html",
			filename: "product.ejs",
			//minify: true
			chunks: ['product']
		}),
		new MiniCssExtractPlugin({
			filename: '[name].css',
			chunkFilename: '[id].css',
		}),
		new VueLoaderPlugin(),
		new CopyPlugin({
			patterns: loadImages("./frontend/images/"),
		}),
	],
};
