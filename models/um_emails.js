'use strict';
module.exports = (sequelize, DataTypes) => {
  const UM_EMAILS = sequelize.define('UM_EMAIL', {
    email: DataTypes.STRING
  }, {});
  UM_EMAILS.associate = function(models) {
    // associations can be defined here
    UM_EMAILS.hasOne(models.UM_USER, {foreignKey: 'emailId', alias: 'user'})
  };
  return UM_EMAILS;
};