'use strict';
module.exports = (sequelize, DataTypes) => {
  const PM_MANUFACTURER = sequelize.define('PM_MANUFACTURER', {
    name: DataTypes.STRING
  }, {});
  PM_MANUFACTURER.associate = function(models) {
    // associations can be defined here
    PM_MANUFACTURER.hasMany(models.PM_PRODUCT, {foreignKey: 'manufacturerId', as: 'products'});
  };
  return PM_MANUFACTURER;
};
