'use strict';
module.exports = (sequelize, DataTypes) => {
  const HP_HEATPUMPSERVICEFILES = sequelize.define('HP_HEATPUMPSERVICEFILE', {
    heatpumpserviceId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'HP_HEATPUMPSERVICE',
        key: 'id'
      }
    },
    fileId:{
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'PM_IMAGE',
        key: 'id'
      }
    },
  }, {});
  HP_HEATPUMPSERVICEFILES.associate = function(models) {
    HP_HEATPUMPSERVICEFILES.belongsTo(models.HP_HEATPUMPSERVICE, {foreignKey: 'heatpumpserviceId', as: 'heatpumpservice'})
    HP_HEATPUMPSERVICEFILES.belongsTo(models.PM_IMAGE, {foreignKey: 'fileId', as: 'file'})
  };
  return HP_HEATPUMPSERVICEFILES;
};
