'use strict';
module.exports = (sequelize, DataTypes) => {
  const PM_TAG = sequelize.define('PM_TAG', {
    name: DataTypes.STRING,
    description: DataTypes.STRING
  }, {});
  PM_TAG.associate = function(models) {
    // associations can be defined here
    PM_TAG.belongsToMany(models.PM_PRODUCT, { through: 'PM_PRODUCTTAG', foreignKey: 'tagId', as: "products"})
    PM_TAG.belongsToMany(models.FM_PAGE, { through: 'FM_PAGETAG', foreignKey: 'tagId', as: "pages"})
  };
  return PM_TAG;
};
