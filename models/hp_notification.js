'use strict';
module.exports = (sequelize, DataTypes) => {
  const HP_NOTIFICATION = sequelize.define('HP_NOTIFICATION', {
    date: DataTypes.DATE,
    heatpumpId:  {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'HP_HEATPUMP',
        key: 'id'
      }
    },
    emailhistoryId:  {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'UM_EMAILHISTORY',
        key: 'id'
      }
    },
    fragmentNotificationId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      references: {
        model: 'HP_FRAGMENTNOTIFICATION',
        key: 'id'
      }
    },
  }, {});
  HP_NOTIFICATION.associate = function(models) {
    HP_NOTIFICATION.belongsTo(models.HP_HEATPUMP, {foreignKey: 'heatpumpId', as: 'heatpump'})
    HP_NOTIFICATION.belongsTo(models.UM_EMAILHISTORY, {foreignKey: 'emailhistoryId', as: 'emailhistory'})
    HP_NOTIFICATION.belongsTo(models.HP_FRAGMENTNOTIFICATION, {foreignKey: 'fragmentNotificationId', as: 'fragmentnotification'})
  };
  return HP_NOTIFICATION;
};
