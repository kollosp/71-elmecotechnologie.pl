'use strict';
module.exports = (sequelize, DataTypes) => {
  const HP_NOTIFICATIONPOLICY = sequelize.define('HP_NOTIFICATIONPOLICY', {
    name: DataTypes.STRING,
    period: DataTypes.INTEGER,
    firstNotification: DataTypes.INTEGER,
    secondNotification: DataTypes.INTEGER,
    thirdNotification: DataTypes.INTEGER
  }, {});
  HP_NOTIFICATIONPOLICY.associate = function(models) {
    // associations can be defined here
  };
  return HP_NOTIFICATIONPOLICY;
};