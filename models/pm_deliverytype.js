'use strict';
module.exports = (sequelize, DataTypes) => {
  const PM_DELIVERYTYPE = sequelize.define('PM_DELIVERYTYPE', {
    name: DataTypes.STRING
  }, {});
  PM_DELIVERYTYPE.associate = function(models) {
    // associations can be defined here
  };
  return PM_DELIVERYTYPE;
};