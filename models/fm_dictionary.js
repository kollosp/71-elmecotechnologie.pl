'use strict';
module.exports = (sequelize, DataTypes) => {
  const FM_DICTIONARY = sequelize.define('FM_DICTIONARY', {
    key: DataTypes.STRING,
    en: DataTypes.TEXT,
    pl: DataTypes.TEXT
  }, {});
  FM_DICTIONARY.associate = function(models) {
    // associations can be defined here
  };
  return FM_DICTIONARY;
};