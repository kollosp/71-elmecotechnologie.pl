'use strict';
module.exports = (sequelize, DataTypes) => {
  const HP_HEATPUMPSERVICE = sequelize.define('HP_HEATPUMPSERVICE', {
    heatpumpId:{
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'HP_HEATPUMP',
        key: 'id'
      }
    },
    serviceTypeId:{
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'HP_HEATPUMPSERVICETYPE',
        key: 'id'
      }
    },
    name: DataTypes.STRING,
    date: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    notes: DataTypes.TEXT
  }, {});
  HP_HEATPUMPSERVICE.associate = function(models) {
    HP_HEATPUMPSERVICE.belongsTo(models.HP_HEATPUMP, {foreignKey: 'heatpumpId', as: 'heatpump'})
    HP_HEATPUMPSERVICE.belongsTo(models.HP_HEATPUMPSERVICETYPE, {foreignKey: 'serviceTypeId', as: 'serviceType'})
  };
  return HP_HEATPUMPSERVICE;
};
