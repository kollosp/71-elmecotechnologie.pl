'use strict';
module.exports = (sequelize, DataTypes) => {
  const PM_PRODUCTFRAGMENT = sequelize.define('PM_PRODUCTFRAGMENT', {
    fragmentId: DataTypes.INTEGER,
    productId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    yIndex: DataTypes.INTEGER,
  }, {});
  PM_PRODUCTFRAGMENT.associate = function(models) {
    // associations can be defined here
    PM_PRODUCTFRAGMENT.belongsTo(models.PM_PRODUCT, {foreignKey: 'productId', as: 'product'})
    PM_PRODUCTFRAGMENT.belongsTo(models.FM_FRAGMENT, {foreignKey: 'fragmentId', as: 'fragment'})
  };
  return PM_PRODUCTFRAGMENT;
};
