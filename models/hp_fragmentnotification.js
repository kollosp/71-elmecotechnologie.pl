'use strict';
module.exports = (sequelize, DataTypes) => {
  const HP_FRAGMENTNOTIFICATION = sequelize.define('HP_FRAGMENTNOTIFICATION', {
    name: DataTypes.STRING,
    fragmentId: DataTypes.INTEGER
  }, {});
  HP_FRAGMENTNOTIFICATION.associate = function(models) {
    HP_FRAGMENTNOTIFICATION.belongsTo(models.FM_FRAGMENT, {foreignKey: 'fragmentId', as: 'fragment'})
  };
  return HP_FRAGMENTNOTIFICATION;
};
