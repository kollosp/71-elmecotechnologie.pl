'use strict';
module.exports = (sequelize, DataTypes) => {
  const PM_PRODUCTTYPE = sequelize.define('PM_PRODUCTTYPE', {
    superTypeId: {
      allowNull: true,
      type: DataTypes.INTEGER,
      references: {
        model: 'PM_PRODUCTTYPE',
        key: 'id'
      }
    },
    name: DataTypes.STRING,
    description: DataTypes.STRING
  }, {});
  PM_PRODUCTTYPE.associate = function(models) {
    // associations can be defined here

    PM_PRODUCTTYPE.hasOne(models.PM_PRODUCTTYPE, {foreignKey: 'superTypeId', as: 'superType'})
    PM_PRODUCTTYPE.belongsTo(models.PM_PRODUCTTYPE, {foreignKey: 'superTypeId', as: 'underTypes'});
    PM_PRODUCTTYPE.hasMany(models.PM_PRODUCT, {foreignKey: 'productTypeId', as: 'products'});
    PM_PRODUCTTYPE.belongsToMany(models.PM_IMAGE, { through: 'PM_PRODUCTTYPEIMAGE', foreignKey: 'productTypeId', as: "images"})
  };
  return PM_PRODUCTTYPE;
};
