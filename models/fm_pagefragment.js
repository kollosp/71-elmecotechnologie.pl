'use strict';
module.exports = (sequelize, DataTypes) => {
  const FM_PAGEFRAGMENT = sequelize.define('FM_PAGEFRAGMENT', {
    name: DataTypes.STRING,
    pageId: DataTypes.INTEGER,
    fragmentId: DataTypes.INTEGER,
    xIndex: DataTypes.INTEGER,
    yIndex: DataTypes.INTEGER,
    available: DataTypes.BOOLEAN
  }, {});
  FM_PAGEFRAGMENT.associate = function(models) {
    // associations can be defined here

    FM_PAGEFRAGMENT.belongsTo(models.FM_PAGE, {foreignKey: 'pageId', as: 'page'})
    FM_PAGEFRAGMENT.belongsTo(models.FM_FRAGMENT, {foreignKey: 'fragmentId', as: 'fragment'})
  };
  return FM_PAGEFRAGMENT;
};
