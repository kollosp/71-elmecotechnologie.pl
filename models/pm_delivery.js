'use strict';
module.exports = (sequelize, DataTypes) => {
  const PM_DELIVERY = sequelize.define('PM_DELIVERY', {
    name: DataTypes.STRING,
    note: DataTypes.TEXT,
    date: DataTypes.DATE,
    userId: DataTypes.INTEGER,
    deliveryDirection: DataTypes.BOOLEAN,
    deliveryTypeId: DataTypes.INTEGER,
    warehouseId: DataTypes.INTEGER
  }, {});
  PM_DELIVERY.associate = function(models) {
    // associations can be defined here
    PM_DELIVERY.belongsToMany(models.PM_PRODUCT, { through: 'PM_PRODUCTDELIVERY', foreignKey: 'deliveryId', as: "products"})
    PM_DELIVERY.belongsTo(models.PM_DELIVERYTYPE, { foreignKey: 'deliveryTypeId', as: "deliveryType"})
    PM_DELIVERY.belongsTo(models.PM_WAREHOUSE, { foreignKey: 'warehouseId', as: "warehouse"})
    PM_DELIVERY.belongsTo(models.UM_USER, { foreignKey: 'userId', as: "user"})
    PM_DELIVERY.hasMany(models.PM_PRODUCTDELIVERY, {foreignKey: 'deliveryId', as: 'productDeliveries'})
  };
  return PM_DELIVERY;
};
