'use strict';
module.exports = (sequelize, DataTypes) => {
    const UM_USER = sequelize.define('UM_USER', {
        loginDataId: {
            type:DataTypes.INTEGER,
            references: {
                model: 'UM_LOGINDATA',
                key: 'id'
            }
        },
        personDataId: {
            type: DataTypes.INTEGER,
            references: {
                model: 'UM_PERSONDATA',
                key: 'id'
            }
        },
        emailId: {
            type: DataTypes.INTEGER,
            references: {
                model: 'UM_EMAIL',
                key: 'id'
            }
        },
        deleted: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: 0,
        }
    }, {});
    UM_USER.associate = function(models) {
        // associations can be defined here
        UM_USER.belongsToMany(models.UM_PERMISSION, { through: 'UM_USERPERMISSION', foreignKey: "userId", as: 'permissions'})

        UM_USER.belongsTo(models.UM_LOGINDATA, {foreignKey: 'loginDataId', as: 'logindata'})
        UM_USER.belongsTo(models.UM_PERSONDATA, {foreignKey: 'personDataId', as: 'persondata'})
        UM_USER.belongsTo(models.UM_EMAIL, {foreignKey: 'emailId', as: 'email'})

        UM_USER.hasMany(models.UM_USERLOG, {foreignKey: 'userId', as: 'logs'})
        //associate with classes management models
    };
    return UM_USER;
};
