'use strict';
module.exports = (sequelize, DataTypes) => {
  const FM_PAGETYPE = sequelize.define('FM_PAGETYPE', {
    name: DataTypes.STRING
  }, {});
  FM_PAGETYPE.associate = function(models) {
    // associations can be defined here
    FM_PAGETYPE.hasMany(models.FM_PAGE, {foreignKey: 'pageTypeId', as: 'pageType'})
  };
  return FM_PAGETYPE;
};
