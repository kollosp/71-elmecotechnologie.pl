'use strict';
module.exports = (sequelize, DataTypes) => {
  const FM_FRAGMENT = sequelize.define('FM_FRAGMENT', {
    key: DataTypes.STRING,
    content: DataTypes.TEXT
  }, {});
  FM_FRAGMENT.associate = function(models) {
    // associations can be defined here
    FM_FRAGMENT.belongsToMany(models.PM_PRODUCT, { through: 'PM_PRODUCTFRAGMENT', foreignKey: 'fragmentId', as: "products"})
    FM_FRAGMENT.belongsToMany(models.FM_PAGE, { through: 'FM_PAGEFRAGMENT', foreignKey: 'fragmentId', as: "pages"})
  };
  return FM_FRAGMENT;
};
