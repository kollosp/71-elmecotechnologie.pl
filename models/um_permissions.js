'use strict';
module.exports = (sequelize, DataTypes) => {
    const UM_PERMISSION = sequelize.define('UM_PERMISSION', {
        accessLevel: DataTypes.INTEGER,
        name: {
            type: DataTypes.STRING,
            unique: true
        }
    }, {});
    UM_PERMISSION.associate = function(models) {
        // associations can be defined here

        UM_PERMISSION.belongsToMany(models.UM_USER, { through: 'UM_USERPERMISSION', foreignKey: 'permissionId'})
    };
    return UM_PERMISSION;
};