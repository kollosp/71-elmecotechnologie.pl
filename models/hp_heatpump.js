'use strict';
module.exports = (sequelize, DataTypes) => {
  const HP_HEATPUMP = sequelize.define('HP_HEATPUMP', {
    productId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'PM_PRODUCT',
        key: 'id'
      }
    },
    customerId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'HP_CUSTOMER',
        key: 'id'
      }
    },
    notificationPolicyId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'HP_NOTIFICATIONPOLICY',
        key: 'id'
      }
    },
    mountedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    notes: DataTypes.TEXT,
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    city:  {
      type: DataTypes.STRING,
      allowNull: false
    },
    code:  {
      type: DataTypes.STRING,
      allowNull: false
    },
    street:  {
      type: DataTypes.STRING,
      allowNull: false
    },
    refrigerantId:  {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'HP_REFRIGERANT',
        key: 'id'
      }
    },
    refrigerantWeight: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    customerNotificationAllowed: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    serviceNotificationAllowed: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
  }, {});
  HP_HEATPUMP.associate = function(models) {
    HP_HEATPUMP.belongsTo(models.PM_PRODUCT, {foreignKey: 'productId', as: 'product'})
    HP_HEATPUMP.belongsTo(models.HP_CUSTOMER, {foreignKey: 'customerId', as: 'customer'})
    HP_HEATPUMP.belongsTo(models.HP_REFRIGERANT, {foreignKey: 'refrigerantId', as: 'refrigerant'})
    HP_HEATPUMP.belongsTo(models.HP_NOTIFICATIONPOLICY, {foreignKey: 'notificationPolicyId', as: 'notificationpolicy'})


    HP_HEATPUMP.hasMany(models.HP_HEATPUMPSERVICE, {foreignKey: 'heatpumpId', as: 'services'})
    HP_HEATPUMP.hasMany(models.HP_NOTIFICATION, {foreignKey: 'heatpumpId', as: 'notifications'})


    HP_HEATPUMP.belongsToMany(models.PM_IMAGE, { through: 'HP_HEATPUMPFILES', foreignKey: 'heatpumpId', as: 'files'})
  };
  return HP_HEATPUMP;
};
