'use strict';
module.exports = (sequelize, DataTypes) => {
  const HP_CUSTOMER = sequelize.define('HP_CUSTOMER', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: ""
    },
    surname: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: false
    },
    altEmail: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {});
  HP_CUSTOMER.associate = function(models) {

    HP_CUSTOMER.hasMany(models.HP_HEATPUMP, {foreignKey: 'customerId', as: 'heatpump'});
  };
  return HP_CUSTOMER;
};
