'use strict';
module.exports = (sequelize, DataTypes) => {
  const UM_USERPERMISSION = sequelize.define('UM_USERPERMISSION', {
    userId: {
      type:DataTypes.INTEGER,
      references: {
        model: 'UM_USER',
        key: 'id'
      }
    },
    permissionId: {
      type:DataTypes.INTEGER,
      references: {
        model: 'UM_PERMISSION',
        key: 'id'
      }
    }
  }, {});
  UM_USERPERMISSION.associate = function(models) {
    // associations can be defined here
    UM_USERPERMISSION.belongsTo(models.UM_USER, {foreignKey: 'userId', as: 'user'})
    UM_USERPERMISSION.belongsTo(models.UM_PERMISSION, {foreignKey: 'permissionId', as: 'permission'})
  };
  return UM_USERPERMISSION;
};
