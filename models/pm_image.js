'use strict';
module.exports = (sequelize, DataTypes) => {
  const PM_IMAGE = sequelize.define('PM_IMAGE', {
    data: DataTypes.BLOB,
    name: DataTypes.STRING,
    alt: DataTypes.STRING,
    extension: DataTypes.STRING,
    isAutoCreated: DataTypes.BOOLEAN
  }, {});
  PM_IMAGE.associate = function(models) {
    // associations can be defined here
    PM_IMAGE.belongsToMany(models.PM_PRODUCT, { through: 'PM_PRODUCTIMAGE', foreignKey: 'imageId', as: "products"})
    PM_IMAGE.belongsToMany(models.PM_PRODUCTTYPE, { through: 'PM_PRODUCTTYPEIMAGE', foreignKey: 'imageId', as: "productTypes"})
  };
  return PM_IMAGE;
};
