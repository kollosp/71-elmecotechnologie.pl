'use strict';
module.exports = (sequelize, DataTypes) => {
  const FM_PAGETAG = sequelize.define('FM_PAGETAG', {
    tagId: DataTypes.INTEGER,
    pageId: DataTypes.INTEGER
  }, {});
  FM_PAGETAG.associate = function(models) {
    // associations can be defined here

    FM_PAGETAG.belongsTo(models.FM_PAGE, {foreignKey: 'pageId', as: 'page'})
    FM_PAGETAG.belongsTo(models.PM_TAG, {foreignKey: 'tagId', as: 'tag'})

  };
  return FM_PAGETAG;
};
