'use strict';
module.exports = (sequelize, DataTypes) => {
  const PM_PRODUCT = sequelize.define('PM_PRODUCT', {
    productTypeId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'PM_PRODUCTTYPE',
        key: 'id'
      }
    },
    qrImageId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'PM_IMAGE',
        key: 'id'
      }
    },
    description: DataTypes.STRING,
    model: DataTypes.STRING, //iMars
    model_2: DataTypes.STRING, //BG9kTR
    shortDesc: DataTypes.STRING,
    manufacturerId: DataTypes.INTEGER, //INVT
    price: DataTypes.FLOAT,
    available: {
      type: DataTypes.INTEGER
    },
    jsonParams: {
      defaultValue: '{}',
      allowNull: false,
      type: DataTypes.STRING
    },
  }, {});
  PM_PRODUCT.associate = function(models) {

    PM_PRODUCT.belongsTo(models.PM_PRODUCTTYPE, {foreignKey: 'productTypeId', as: 'productType'})
    PM_PRODUCT.belongsTo(models.PM_MANUFACTURER, {foreignKey: 'manufacturerId', as: 'manufacturer'})
    PM_PRODUCT.belongsTo(models.PM_IMAGE, { foreignKey: 'qrImageId', as: "qrImage"})

    PM_PRODUCT.hasMany(models.PM_PRODUCTATTRIBUTE, {foreignKey: 'productId', as: 'attributes'})
    PM_PRODUCT.hasMany(models.PM_PRODUCTDELIVERY, {foreignKey: 'productId', as: 'productDeliveries'})
    PM_PRODUCT.belongsToMany(models.PM_TAG, { through: 'PM_PRODUCTTAG', foreignKey: 'productId', as: "tags"})
    PM_PRODUCT.belongsToMany(models.PM_DELIVERY, { through: 'PM_PRODUCTDELIVERY', foreignKey: 'productId', as: "deliveries"})
    PM_PRODUCT.belongsToMany(models.PM_IMAGE, { through: 'PM_PRODUCTIMAGE', foreignKey: 'productId', as: "images"})
    PM_PRODUCT.belongsToMany(models.FM_FRAGMENT, { through: 'PM_PRODUCTFRAGMENT', foreignKey: 'productId', as: 'fragments'})
  };
  return PM_PRODUCT;
};
