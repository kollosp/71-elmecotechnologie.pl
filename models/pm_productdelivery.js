'use strict';
module.exports = (sequelize, DataTypes) => {
  const PM_PRODUCTDELIVERY = sequelize.define('PM_PRODUCTDELIVERY', {
    productId: DataTypes.INTEGER,
    deliveryId: DataTypes.INTEGER,
    productPrice: DataTypes.FLOAT,
    productCount: DataTypes.FLOAT
  }, {});
  PM_PRODUCTDELIVERY.associate = function(models) {
    // associations can be defined here
    //it depends on deliveryType
    PM_PRODUCTDELIVERY.belongsTo(models.PM_DELIVERY, {foreignKey: 'deliveryId', as: 'delivery'})

    PM_PRODUCTDELIVERY.belongsTo(models.PM_PRODUCT, {foreignKey: 'productId', as: 'product'})
  };
  return PM_PRODUCTDELIVERY;
};
