'use strict';
module.exports = (sequelize, DataTypes) => {
  const UM_LOGINDATA = sequelize.define('UM_LOGINDATA', {
      username: DataTypes.STRING,
      password: DataTypes.STRING,
      token: DataTypes.STRING,
      tokenExpire: DataTypes.DATE,
      lastLogin: DataTypes.DATE,
      accessToken: DataTypes.STRING
  }, {});
  UM_LOGINDATA.associate = function(models) {
    // associations can be defined here
    UM_LOGINDATA.hasOne(models.UM_USER, {foreignKey: 'loginDataId'})
  };
  return UM_LOGINDATA;
};