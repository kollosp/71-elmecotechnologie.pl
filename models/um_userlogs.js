'use strict';
module.exports = (sequelize, DataTypes) => {
  const UM_USERLOG = sequelize.define('UM_USERLOG', {
    userId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'UM_USER',
        key: 'id'
      }
    }
  }, {});
  UM_USERLOG.associate = function(models) {

    UM_USERLOG.belongsTo(models.UM_USER, {foreignKey: 'userId', as: 'user'})
  };
  return UM_USERLOG;
};
