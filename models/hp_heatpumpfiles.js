'use strict';
module.exports = (sequelize, DataTypes) => {
  const HP_HEATPUMPFILES = sequelize.define('HP_HEATPUMPFILES', {
    heatpumpId:  {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'HP_HEATPUMP',
        key: 'id'
      }
    },
    fileId:  {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'PM_IMAGE',
        key: 'id'
      }
    },
  }, {});
  HP_HEATPUMPFILES.associate = function(models) {
    HP_HEATPUMPFILES.belongsTo(models.HP_HEATPUMP, {foreignKey: 'heatpumpId', as: 'heatpump'})
    HP_HEATPUMPFILES.belongsTo(models.PM_IMAGE, {foreignKey: 'imageId', as: 'file'})
  };
  return HP_HEATPUMPFILES;
};
