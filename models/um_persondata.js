'use strict';
module.exports = (sequelize, DataTypes) => {
  const UM_PERSONDATA = sequelize.define('UM_PERSONDATA', {
    name: DataTypes.STRING,
    surname: DataTypes.STRING,
    dateOfBirth: DataTypes.DATE,
    phone: DataTypes.STRING
  }, {});
  UM_PERSONDATA.associate = function(models) {
    // associations can be defined here

    UM_PERSONDATA.hasOne(models.UM_USER, {foreignKey: 'personDataId'})
  };
  return UM_PERSONDATA;
};