'use strict';
module.exports = (sequelize, DataTypes) => {
  const HP_HEATPUMPSERVICETYPE = sequelize.define('HP_HEATPUMPSERVICETYPE', {
    name: DataTypes.STRING,
    description:DataTypes.TEXT
  }, {});
  HP_HEATPUMPSERVICETYPE.associate = function(models) {
    // associations can be defined here
  };
  return HP_HEATPUMPSERVICETYPE;
};
