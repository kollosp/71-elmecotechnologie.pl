'use strict';
module.exports = (sequelize, DataTypes) => {
  const UM_EMAILHISTORY = sequelize.define('UM_EMAILHISTORY', {
    date: DataTypes.DATE,
    subject: DataTypes.STRING,
    receivers: DataTypes.STRING,
    status: DataTypes.INTEGER,
    content: DataTypes.TEXT,
    statusDescription: DataTypes.TEXT
  }, {});
  UM_EMAILHISTORY.associate = function(models) {
    // associations can be defined here
  };
  return UM_EMAILHISTORY;
};