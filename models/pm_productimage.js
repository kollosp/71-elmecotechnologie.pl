'use strict';
module.exports = (sequelize, DataTypes) => {
  const PM_PRODUCTIMAGE = sequelize.define('PM_PRODUCTIMAGE', {
    productId: DataTypes.INTEGER,
    imageId: DataTypes.INTEGER,
    index: DataTypes.INTEGER,
  }, {});
  PM_PRODUCTIMAGE.associate = function(models) {
    PM_PRODUCTIMAGE.belongsTo(models.PM_PRODUCT, {foreignKey: 'productId', as: 'product'})
    PM_PRODUCTIMAGE.belongsTo(models.PM_IMAGE, {foreignKey: 'imageId', as: 'image'})
  };
  return PM_PRODUCTIMAGE;
};
