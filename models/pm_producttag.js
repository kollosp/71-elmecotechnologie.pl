'use strict';
module.exports = (sequelize, DataTypes) => {
  const PM_PRODUCTTAG = sequelize.define('PM_PRODUCTTAG', {
    productId: DataTypes.INTEGER,
    tagId: DataTypes.INTEGER
  }, {});
  PM_PRODUCTTAG.associate = function(models) {
    // associations can be defined here
    PM_PRODUCTTAG.belongsTo(models.PM_PRODUCT, {foreignKey: 'productId', as: 'product'})
    PM_PRODUCTTAG.belongsTo(models.PM_TAG, {foreignKey: 'tagId', as: 'tag'})
  };
  return PM_PRODUCTTAG;
};
