'use strict';
module.exports = (sequelize, DataTypes) => {
  const PM_WAREHOUSE = sequelize.define('PM_WAREHOUSE', {
    name: DataTypes.STRING
  }, {});
  PM_WAREHOUSE.associate = function(models) {
    // associations can be defined here
  };
  return PM_WAREHOUSE;
};