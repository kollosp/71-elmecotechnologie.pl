'use strict';
module.exports = (sequelize, DataTypes) => {
  const PM_PRODUCTTYPEIMAGE = sequelize.define('PM_PRODUCTTYPEIMAGE', {
    productTypeId: DataTypes.INTEGER,
    imageId: DataTypes.INTEGER,
    index: DataTypes.INTEGER
  }, {});
  PM_PRODUCTTYPEIMAGE.associate = function(models) {
    PM_PRODUCTTYPEIMAGE.belongsTo(models.PM_PRODUCTTYPE, {foreignKey: 'productTypeId', as: 'productType'})
    PM_PRODUCTTYPEIMAGE.belongsTo(models.PM_IMAGE, {foreignKey: 'imageId', as: 'image'})
  };
  return PM_PRODUCTTYPEIMAGE;
};
