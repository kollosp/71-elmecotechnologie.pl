'use strict';
module.exports = (sequelize, DataTypes) => {
  const HP_REFRIGERANT = sequelize.define('HP_REFRIGERANT', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
      unique: true
    },
    chemicalName: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: ""
    },
    gwp: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    }
  }, {});
  HP_REFRIGERANT.associate = function(models) {
    // associations can be defined here
  };
  return HP_REFRIGERANT;
};
