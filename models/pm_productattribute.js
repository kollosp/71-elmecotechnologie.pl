'use strict';
module.exports = (sequelize, DataTypes) => {
  const PM_PRODUCTATTRIBUTE = sequelize.define('PM_PRODUCTATTRIBUTE', {
    productId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    value: DataTypes.STRING,
    yIndex: DataTypes.INTEGER,
  }, {});
  PM_PRODUCTATTRIBUTE.associate = function(models) {
    // associations can be defined here
    PM_PRODUCTATTRIBUTE.belongsTo(models.PM_PRODUCT, {foreignKey: 'productId', as: 'product'})
  };
  return PM_PRODUCTATTRIBUTE;
};
