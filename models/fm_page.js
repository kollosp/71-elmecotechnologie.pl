'use strict';
module.exports = (sequelize, DataTypes) => {
  const FM_PAGE = sequelize.define('FM_PAGE', {
    name: DataTypes.STRING,
    htmlTitle: DataTypes.STRING,
    htmlDescription: DataTypes.STRING,
    htmlTags: DataTypes.STRING,
    url: DataTypes.STRING,
    available: DataTypes.BOOLEAN,
    pageTypeId: DataTypes.INTEGER,
    htmlTemplate: DataTypes.TEXT,
    htmlTemplateFilePath: DataTypes.STRING,
    predictEnable: DataTypes.BOOLEAN,
    tableOfContentsEnable: DataTypes.BOOLEAN
  }, {});
  FM_PAGE.associate = function(models) {
    // associations can be defined here

    FM_PAGE.belongsTo(models.FM_PAGETYPE, {foreignKey: 'pageTypeId', as: 'pageType'})

    FM_PAGE.belongsToMany(models.PM_TAG, { through: 'FM_PAGETAG', foreignKey: 'pageId', as: "tags"})
    FM_PAGE.belongsToMany(models.FM_FRAGMENT, { through: 'FM_PAGEFRAGMENT', foreignKey: 'pageId', as: 'fragments'})
  };
  return FM_PAGE;
};
