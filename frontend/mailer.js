import Vue from "vue";
import mailer from "./assets/mailer.vue";
import GlobalFunctions from "./plugins/globalFunctions";
Vue.use(GlobalFunctions)
let m = new Vue({
    el: "#mailer",
    components: {mailer},
    render: h => h(mailer)
})
