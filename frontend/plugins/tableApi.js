export default {
    data() {
        return {
            dbGlobalMessageRef: null
        }
    },
    methods: {
        dbSetMessageRef: function (messageRef){
            this.dbGlobalMessageRef = messageRef
        },

        dbRemove: function({table, id}){
            return new Promise((resolve, reject) => {
                if(confirm("Czy na pewno usunąć wiersz?")) {
                    this.axiosPostMsg('/tablemanagement/remove', {
                        table: table,
                        id: id
                    }).then(m => {
                        this.dbShowSuccess("", "Poprawnie usunięto wiersz z bazy danych")
                        resolve()
                    }).catch(e => {
                        if(!e.match(/axios busy/)){
                            console.error(e)
                        }else{
                            this.dbShowDanger("", "Nie można usunąć wiersza. Wiersz pozostaje w relacji z innym wierszem w bazie danych")
                        }
                        reject(e)
                    })
                }
            })
        },

        dbUpdate: function ({table, id, data}){
            return new Promise((resolve, reject) => {
                this.axiosPostMsg('/tablemanagement/updateorinsert', {
                    table,
                    id,
                    data
                }).then((m) => {
                    resolve(m.id)
                }).catch(e => {
                    if(!e.toString().match(/axios busy/)){
                        console.error(e)
                    }else{
                        this.dbShowDanger("", "Błąd podczas wprowadzania danych. Upewnij się, że wszystkie pola zostały poprawnie uzupełnione.")
                    }
                    reject(e)
                })
            })
        },

        dbFindAllIds: function ({table, constraints= []}){
            return this.axiosPostMsg('/tablemanagement/findAll',{
                table,
                constraints,
                attributes: ['id', 'name']
            }).then(m => {
                return m
            }).catch(()=>{})
        },

        dbFindAll: function ({table, constraints = []}){
            return this.axiosPostMsg('/tablemanagement/findAll',{
                table,
                constraints
            }).then(m => {
                return m
            }).catch(()=>{})
        },

        dbFindOne:function ({table, id}){
            return new Promise((resolve, reject) => {
                this.axiosPostMsg('/tablemanagement/findone', {
                    table,
                    id
                }).then(m => {
                    resolve(m)
                })
            })
        },

        dbShowDanger: function (title, txt){
            if(this.dbGlobalMessageRef){
                this.dbGlobalMessageRef.showDanger(title, txt)
            }
            console.error(title, txt)
        },

        dbShowSuccess: function (title, txt){
            if(this.dbGlobalMessageRef){
                this.dbGlobalMessageRef.showSuccess(title, txt)
            }
            console.log(title, txt)
        }
    }
}
