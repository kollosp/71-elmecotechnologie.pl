import axios from 'axios'

export default {
    install(Vue, options) {
        Vue.directive('click-outside', {
            bind: function (el, binding, vnode) {
                el.clickOutsideEvent = function (event) {
                    // here I check that click was outside the el and his children
                    if (!(el == event.target || el.contains(event.target))) {
                        // and if it did, call method provided in attribute value
                        vnode.context[binding.expression](event);
                    }
                };
                document.body.addEventListener('click', el.clickOutsideEvent)
            },
            unbind: function (el) {
                document.body.removeEventListener('click', el.clickOutsideEvent)
            },
        });

        Vue.mixin({
            data() {
                return {
                    loggedUser: {},
                    intervals: [],
                    timeouts: [],
                    axiosPostMsgBusy: {}, //object store busy status of every endpoint based on url and data
                }
            },

            beforeRouteLeave: function(to, from, next) {
                //destroy set intervals and timeouts to avoid running them after component switch
                this.destroyTimeouts()
                this.destroyIntervals()
                next()
            },

            methods: {
                isDeepEqual(object1, object2) {

                    const objKeys1 = Object.keys(object1);
                    const objKeys2 = Object.keys(object2);

                    if (objKeys1.length !== objKeys2.length) return false;

                    for (let key of objKeys1) {
                        const value1 = object1[key];
                        const value2 = object2[key];

                        const isObjects = this.isDeepEqualIsObject(value1) && this.isDeepEqualIsObject(value2);

                        if ((isObjects && !this.isDeepEqual(value1, value2)) ||
                            (!isObjects && value1 !== value2)
                        ) {
                            return false;
                        }
                    }
                    return true;
                },

                isDeepEqualIsObject (object) {
                    return object != null && typeof object === "object";
                },

                isMobile() {
                    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                        return true
                    } else {
                        return false
                    }
                },
                showDanger(title, content){
                    if(this.$root.$refs.messageBox){
                        this.$root.$refs.messageBox.showDanger(title, content)
                    }else{
                        console.error("Root element has no messageBox child")
                    }
                },
                showSuccess(title, content){
                    if(this.$root.$refs.messageBox){
                        this.$root.$refs.messageBox.showSuccess(title, content)
                    }else{
                        console.error("Root element has no messageBox child")
                    }
                },
                showInfo(title, content){
                    if(this.$root.$refs.messageBox){
                        this.$root.$refs.messageBox.showInfo(title, content)
                    }else{
                        console.error("Root element has no messageBox child")
                    }
                },

                setDictionary(dictionary){
                    this.$root.$data.$_boomeko_dictionary = dictionary
                },

                loadDictionary(url){
                    this.axiosPostMsg(url,{
                        lang: 'pl'
                    }).then(m => {
                        this.setDictionary(m)
                    })
                },

                translate: function(key){
                    if(!this.$root.$data.$_boomeko_dictionary) return key
                    return this.$root.$data.$_boomeko_dictionary[key] || key
                },

                destroyIntervals : function(){
                    console.log(`Destroying intervals. To be des: ${JSON.stringify(this.intervals)}`)
                    for(let i in this.intervals){
                        clearInterval(this.intervals[i])
                    }
                    this.intervals = []
                },

                destroyTimeouts : function(){
                    for(let i in this.timeouts){
                        clearTimeout(this.timeouts[i])
                    }
                    this.timeouts = []
                },

                setInterval: function(callback, time){
                    this.intervals.push(setInterval(callback, time))
                    console.log(`new interval created. Execution every ${time} ms. id: ${this.intervals[this.intervals.length-1]}`)
                },

                setTimeout: function(callback, time){
                    this.timeouts.push(setTimeout(callback, time))
                },

                /**
                 * Function checks if each attribute given in attr exists, to avoid "can not read property of undefined"
                 * exception
                 * @param obj
                 * @param attr attributes list
                 * @param i current attribute (recurrence counter)
                 * @returns {string|any}
                 */
                checkObj: function(obj, attr, i = 0) {
                    if (i == attr.length) {
                        return obj
                    } else {
                        if (obj == undefined || obj == null) {
                            return "---"
                        } else {
                            return this.checkObj(obj[attr[i]], attr, i + 1)
                        }
                    }
                },

                /**
                 * Function allow to make a post query do server. It handle all standard errors. Those made by server query, which are
                 * interpreted as correct response (code 200) and error message (other codes). It catches errors and log them to
                 * the console
                 * @param url - query url
                 * @param payload - body params
                 * @returns {Promise<unknown>} - only resolve
                 */
                axiosPostMsg(url, payload){
                    let self = this
                    return new Promise(((resolve, reject) => {
                        let key = url + JSON.stringify(payload)

                        if(self.axiosPostMsgBusy[key] === true){
                            //console.warn(`Axios busy at request ${key}`)
                            reject(new Error(`Axios busy at request ${key}`))
                            return
                        }

                        //set busy flag
                        self.axiosPostMsgBusy[key] = true

                        axios.post(url, payload).then(m => {
                            if(m.data.error){
                                this.showDanger("Błąd serwera", `Nastąpił nieoczekiwany błąd. <br> ${m.data.error.message} <br> ${m.data.error.name}. Szczegóły dostęne w konsoli.`)
                                console.error("Std. error at ", key)
                                console.error(m.data.error)
                                if(m.data.error == "User logged out") {
                                    this.$root.catch401Error()
                                    return
                                }

                                reject(m.data.error)
                            }else{
                                resolve(m.data)
                            }
                            //free the object
                            delete self.axiosPostMsgBusy[key]

                        }).catch(e => {
                            console.error(`Comm exception: (${e.response.status})`, e)
                            this.showDanger("Błąd komunikacji", `Nastąpił nieoczekiwany błąd. <br> Kod błędu: ${e.response.status}. Szczegóły dostęne w konsoli.`)
                            switch (e.response.status) {
                                case 401:
                                    console.log("401 error, session has been ended")
                                    this.$root.catch401Error()
                                    break;
                            }
                            //free the object
                            delete self.axiosPostMsgBusy[key]
                        })
                    }))
                },

                /**
                 * Function allows to check if currently logged user has specified permission
                 * @param perm
                 * @returns {boolean}
                 */
                checkUserPermission(perm) {
                    if(this.loggedUser && this.loggedUser.permissions){
                        for(let p of this.loggedUser.permissions)
                            if(p == perm)
                                return true
                    }

                    return false
                },

                /**
                 * Function sets user data, which will be used to determinate access level
                 * @param userData
                 */
                setUser(userData){
                    this.loggedUser = userData
                    if(!this.loggedUser.permissions) this.loggedUser = []
                },

                /**
                 * Function return date in format DD.MM.YY HH:MM:SS
                 * @param date
                 */
                renderDate: function (date) {
                    date = new Date(date)
                    return date.getDate().toString().padStart(2, '0') + "." + (date.getMonth() +1).toString().padStart(2, '0') + "." +
                        date.getFullYear().toString().padStart(2, '0') + " " + date.getHours().toString().padStart(2, '0') + ":" +
                        date.getMinutes().toString().padStart(2, '0') + ":" + date.getSeconds().toString().padStart(2, '0')
                },

                renderTime: function (date) {
                    date = new Date(date)
                    return date.getHours().toString().padStart(2, '0') + ":" + date.getMinutes().toString().padStart(2, '0')
                        + ":" + date.getSeconds().toString().padStart(2, '0')
                },

                renderOnlyDate: function (date) {
                    date = new Date(date)
                    return date.getDate().toString().padStart(2, '0') + "." + (date.getMonth() +1).toString().padStart(2, '0') + "." +
                        date.getFullYear().toString().padStart(2, '0')
                },

                renderString:function (str, maxLen=25) {
                    if(str && str.length > maxLen){
                        return str.slice(0,maxLen-3) + "..."
                    }else{
                        return str
                    }
                },

                /**
                 * Function translate date into date input component format yyyy-MM-ddThh:mm
                 * @param date
                 */
                toDateInputFormat(date){
                    date = new Date(date)
                    return date.getFullYear().toString().padStart(4, '0') + "-" + (date.getMonth() +1).toString().padStart(2, '0') + "-" +
                        date.getDate().toString().padStart(2, '0') + "T" + date.getHours().toString().padStart(2, '0') + ":" +
                        date.getMinutes().toString().padStart(2, '0') + ":" + date.getSeconds().toString().padStart(2, '0')
                },

                fromDateInputFormat(str){
                    return new Date(Date.parse(str))
                },

                format(a, precision=2){
                    if(isNaN(a)){
                        return "---"
                    }

                    try{
                        return a.toFixed(precision)
                    }catch (e) {
                        return a
                    }
                },

                brokenConnection: function (pingAt, seconds=3600*24*3) {
                    let d = new Date(pingAt)
                    //console.log(pingAt, Date.now(), d.getTime(), (Date.now() - d.getTime()) > 3600*24*3*1000)
                    if((Date.now() - d.getTime()) > seconds*1000){
                        return true
                    }
                    return false
                }
            }
        })
    },
}
