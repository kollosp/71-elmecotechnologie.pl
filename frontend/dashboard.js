import './stylesheets/style.sass'
//import './views/login.html';


//library
import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'
import Vue from "vue";
import VueRouter from "vue-router";
import wysiwyg from "vue-wysiwyg";

//
import GlobalFunctions from "./plugins/globalFunctions";
import loginView from "./pages/login/index.vue";
import Template from "./pages/dashboard/template.vue";
import dashboard from "./pages/dashboard/dashboard.vue";
import databaseView from "./pages/dashboard/viewModules/databaseView.vue";

import userDatabaseTable from "./pages/dashboard/databaseInterface/userDatabaseTable.vue";
import userDatabaseTableMain from "./pages/dashboard/databaseInterface/table/userDatabaseTableMain.vue";
import userDatabaseTableEdit from "./pages/dashboard/databaseInterface/table/userDatabaseTableEdit.vue";
import userDatabaseTableBrief from "./pages/dashboard/databaseInterface/table/userDatabaseTableBrief.vue";
import userDatabaseMain from "./pages/dashboard/databaseInterface/userDatabaseMain.vue";
import mainView from "./pages/dashboard/viewModules/mainView.vue";
import cmsView from "./pages/dashboard/viewModules/cmsView.vue";
import cmsMainView from "./pages/dashboard/viewModules/cmsView/main.vue";
import treeView from "./pages/dashboard/databaseTreeViewInterface/treeView.vue";
import warehouseView from "./pages/dashboard/viewModules/warehouseView.vue";
import warehouseViewMain from "./pages/dashboard/viewModules/warehouseView/warehouseViewMain.vue";
import warehouseViewOverview from "./pages/dashboard/viewModules/warehouseView/warehouseViewOverview.vue";
import warehouseViewPickup from "./pages/dashboard/viewModules/warehouseView/warehouseViewPickup.vue";
import warehouseViewDelivery from "./pages/dashboard/viewModules/warehouseView/warehouseViewDelivery.vue";
import warehouseViewInventory from "./pages/dashboard/viewModules/warehouseView/warehouseViewInventory.vue";
import warehouseViewQrGenerator from "./pages/dashboard/viewModules/warehouseView/warehouseViewQrGenerator.vue";
import cmsViewImages from "./pages/dashboard/viewModules/cmsView/cmsViewImages.vue";
import cmsViewProducts from "./pages/dashboard/viewModules/cmsView/cmsViewProducts.vue";
import cmsViewProductsPreview from "./pages/dashboard/viewModules/cmsView/cmsViewProducts/cmsViewProductsPreview.vue";
import warehouseViewDeliveryPreview
    from "./pages/dashboard/viewModules/warehouseView/warehouseViewDelivery/warehouseViewDeliveryPreview.vue";
import warehouseViewPickUpPreview
    from "./pages/dashboard/viewModules/warehouseView/warehouseViewDelivery/warehouseViewPickUpPreview.vue";
import userView from "./pages/dashboard/viewModules/userView.vue";
import userViewMain from "./pages/dashboard/viewModules/userView/userViewMain.vue";
import userViewUserPreview from "./pages/dashboard/viewModules/userView/userViewUserPreview.vue";
import warehouseViewWarehouse from "./pages/dashboard/viewModules/warehouseView/warehouseViewWarehouses.vue";
import refrigerationView from "./pages/dashboard/viewModules/refrigerationView.vue";
import refrigerationViewMain from "./pages/dashboard/viewModules/refrigerationView/refrigerationViewMain.vue";
import refrigerationViewNotification
    from "./pages/dashboard/viewModules/refrigerationView/refrigerationViewNotification.vue";
import refrigerationViewMainPreview
    from "./pages/dashboard/viewModules/refrigerationView/refrigerationViewMain/refrigerationViewMainPreview.vue";
import refrigerationViewCustomer from "./pages/dashboard/viewModules/refrigerationView/refrigerationViewCustomer.vue";
import refrigerationViewCustomerPreview
    from "./pages/dashboard/viewModules/refrigerationView/refrigerationViewCustomer/refrigerationViewCustomerPreview.vue";
import refrigerationViewComing from "./pages/dashboard/viewModules/refrigerationView/refrigerationViewComing.vue";
import refrigerationViewMainServicePreview
    from "./pages/dashboard/viewModules/refrigerationView/refrigerationViewMain/refrigerationViewMainServicePreview.vue";
import billView from "./pages/dashboard/viewModules/billView.vue";
import billViewMain from "./pages/dashboard/viewModules/billView/billViewMain.vue";
import billViewMainList from "./pages/dashboard/viewModules/billView/billViewMain/billViewMainList.vue";
import billViewMainListBill
    from "./pages/dashboard/viewModules/billView/billViewMain/billViewMainList/billViewMainListBill.vue";

Vue.use(VueRouter)
Vue.use(GlobalFunctions)
Vue.use(VueSidebarMenu)
Vue.use(wysiwyg, {})

let userRouter = new VueRouter({
    routes: [{
        path: '/login', component: loginView
    },{
        path: '/', component: dashboard, children: [{
            path: '/', component: mainView
        },{
            path: '/database', component: databaseView, children: [{
                path: '/', component: userDatabaseMain
            },{
                path: '/database/:table', component: userDatabaseTable, children: [{
                    path: '/', component: userDatabaseTableMain
                },{
                    path: '/database/:table/edit/:id', component: userDatabaseTableEdit
                },{
                    path: '/database/:table/brief', component: userDatabaseTableBrief
                }]
            }]
        },{
            path: '/fragments', component:  cmsView, children: [{
                path: '/', component: cmsMainView
            },{
                path: '/fragments/images', component: cmsViewImages
            },{
                path: '/fragments/products', component: cmsViewProducts
            },{
                path: '/fragments/products/:id', component: cmsViewProductsPreview
            },{
                path: '/fragments/:tree*', component: treeView
            }]
        },{
            path: '/warehouse', component:  warehouseView, children: [{
                path: '/warehouse', component: warehouseViewWarehouse
            }, {
                path: '/warehouse/qr', component: warehouseViewQrGenerator
            }, {
                path: '/warehouse/:warehouseId', component: warehouseViewMain, children: [{
                    path: '/warehouse/:warehouseId', component: warehouseViewOverview
                }, {
                    path: '/warehouse/:warehouseId/pickup', component: warehouseViewPickup
                }, {
                    path: '/warehouse/:warehouseId/pickup/:id', component: warehouseViewPickUpPreview
                }, {
                    path: '/warehouse/:warehouseId/delivery', component: warehouseViewDelivery
                }, {
                    path: '/warehouse/:warehouseId/delivery/:id', component: warehouseViewDeliveryPreview
                }, {
                    path: '/warehouse/:id/inventory', component: warehouseViewInventory
                }]
            }]
        },{
            path: '/users', component:  userView, children: [{
                path: '/', component: userViewMain
            },{
                path: '/users/:id', component: userViewUserPreview
            }]
        },{
            path: '/refrigeration', component: refrigerationView, children: [{
                path: '/refrigeration', component: refrigerationViewMain
            },{
                path: '/refrigeration/notification', component: refrigerationViewNotification
            },{
                path: '/refrigeration/coming', component: refrigerationViewComing
            },{
                path: '/refrigeration/customer', component: refrigerationViewCustomer
            },{
                path: '/refrigeration/customer/:id', component: refrigerationViewCustomerPreview
            },{
                path: '/refrigeration/:heatpumpid/service/:id', component: refrigerationViewMainServicePreview
            },{
                path: '/refrigeration/:id', component: refrigerationViewMainPreview
            }]
        },{
            path: '/billing', component: billView, children: [{
                path: '/billing', component: billViewMain
            },{
                path: '/billing/list/:id', component: billViewMainList
            },{
                path: '/billing/list/:listid/bill/:id', component: billViewMainListBill
            }]
        }]
    }]
})

let mainApp = new Vue({
    el: "#app",
    mixins: [Template],
    router: userRouter,
})
