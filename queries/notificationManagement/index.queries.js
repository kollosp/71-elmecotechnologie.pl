
module.exports = function(ctx) {
    return {
        insertHistory: async function({heatpumpId, fragmentNotificationId, emailhistoryId, date}) {
            let db = ctx.getSequelize()
            let notification = await db.HP_NOTIFICATION.create({
                heatpumpId,
                fragmentNotificationId,
                emailhistoryId,
                date
            })

            return notification
        }
    }
}
