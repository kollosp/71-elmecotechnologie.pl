
module.exports = function(ctx) {
    return {
        findPage: async function({url}) {
            let db = ctx.getSequelize()
            let page = await db.FM_PAGE.findOne({
                where: {
                    url
                },
                include: [{
                    model: db.PM_TAG,
                    as: "tags",
                    attributes: ["name"]
                },{
                    model: db.FM_FRAGMENT,
                    as: "fragments",
                    attributes: ["key", "content"]
                },{
                    model: db.FM_PAGETYPE,
                    as: "pageType",
                    attributes: ["name"]
                }]
            })

            //if no page under given url
            if(page == null) return null

            return page.toJSON()
        },

        findAllPages: async function() {
            let db = ctx.getSequelize()
            let pages = await db.FM_PAGE.findAll({
                include: [{
                    model: db.PM_TAG,
                    as: "tags",
                    attributes: ["name"]
                },{
                    model: db.FM_FRAGMENT,
                    as: "fragments",
                    attributes: ["key", "content"]
                },{
                    model: db.FM_PAGETYPE,
                    as: "pageType",
                    attributes: ["name"]
                }]
            })

            return pages.map((val) => {return val.toJSON()})
        }
    }
}
