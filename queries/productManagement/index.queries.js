const QRCode = require('qrcode')
const {reject} = require("bcrypt/promises");

const genQRCode = function (str){
    return new Promise((resolve, reject) => {
        QRCode.toDataURL(str, { version: 2 }, function (err, url) {
            if(err){
                reject(err)
                return
            }

            //url[0] = format, url[1] = data
            //data:image/png;base64,iVBORw0(...)==
            url = url.split(',')
            let colonIndex = url[0].indexOf(':')
            let semicolonIndex = url[0].indexOf(';')
            let format = url[0].slice(colonIndex+1, semicolonIndex)

            let extension = format.slice(format.indexOf("/")+1)
            //console.log(imageName, format, url[1])
            resolve({format, extension, imageBase64: url[1]})
        })
    })
}

module.exports = function(ctx) {
    let queries = {
        findSubtypesOf: async function({typeName}) {
            let db = ctx.getSequelize()

            let superType = await db.PM_PRODUCTTYPE.findOne({
                where: {
                    name: typeName
                },
                attributes: ['id']
            })


            let subtypes = await db.PM_PRODUCTTYPE.findAll({
                attributes: ['name'],
                where: {
                    superTypeId: superType.id
                },
                include: [{
                    model: db.PM_IMAGE,
                    as: "images",
                    attributes: ['name', 'alt']
                }],
            })

            return subtypes.map(value => {return value.toJSON()})
        },
        findRootTypes: async function() {
            let db = ctx.getSequelize()
            let subtypes = await db.PM_PRODUCTTYPE.findAll({
                attributes: ['name'],
                where: {
                    superTypeId: null
                },
                include: [{
                    model: db.PM_IMAGE,
                    as: "images",
                    attributes: ['name', 'alt']
                }]
            })

            return subtypes.map(value => {return value.toJSON()})
        },

        findProductsOfType: async function({typeName}) {
            let db = ctx.getSequelize()
            let products = await db.PM_PRODUCT.findAll({
                attributes: [['shortDesc', 'name'], 'model', 'model_2', 'price', 'available', 'description'],
                include: [{
                    model: db.PM_PRODUCTTYPE,
                    as: "productType",
                    attributes: [],
                    where: {
                        name: typeName
                    }
                },{
                    model: db.PM_PRODUCTATTRIBUTE,
                    as: "attributes",
                    attributes:['name', 'value']
                },{
                    model: db.PM_TAG,
                    as: "tags",
                    attributes:['name']
                },{
                    model: db.PM_MANUFACTURER,
                    as: "manufacturer",
                },{
                    model: db.PM_IMAGE,
                    as: "images",
                    attributes: ['name', 'alt']
                }],
            })


            return products.map((val) => {return val.toJSON()})
        },

        findProductName: async function({productId}) {
            let db = ctx.getSequelize()
            let data = await db.PM_PRODUCT.findOne({
                where: {
                    id: productId
                },
                attributes: ['shortDesc']
            })

            if(data){
                return data.shortDesc
            }else{
                return ""
            }
        },

        findProduct: async function({productName}) {
            let db = ctx.getSequelize()
            let product = await db.PM_PRODUCT.findOne({
                where: {
                    shortDesc: productName
                },
                include: [{
                    model: db.PM_PRODUCTTYPE,
                    as: "productType",
                },{
                    model: db.PM_MANUFACTURER,
                    as: "manufacturer",
                },{
                    model: db.PM_TAG,
                    as: "tags"
                },{
                    model: db.PM_PRODUCTATTRIBUTE,
                    as: "attributes"
                },{
                    model: db.FM_FRAGMENT,
                    as: "fragments"
                },{
                    model: db.PM_IMAGE,
                    as: "images",
                    attributes: ['name', 'alt']
                }]
            })

            return product ? product.toJSON() : product
        },

        setProductQrCode: function({productId, imageId}){
            let db = ctx.getSequelize()
            return db.PM_PRODUCT.update({
                qrImageId:imageId
            },{
                where: {
                    id: productId
                }
            })
        },

        generateQrCode: async function ({productId}){
            let productName = await queries.findProductName({productId})
            let qrdata = productId

            let qr = await genQRCode(JSON.stringify(qrdata))
            let fileName = "QRCODE_PRODUCT_" + productId + "." + qr.extension
            let buffer = new Buffer(qr.imageBase64, 'base64')

            let file = await ctx.getQueries().fileManagement.updateOrCreateFile({
                fileName,
                format: qr.format,
                data: buffer,
                extension: qr.extension,
                isAutoCreated: true
            })

            let relation = await queries.setProductQrCode({
                productId,
                imageId: file.id
            })
            //console.log("relation", file, {productId, imageId: file.id}, relation)
            return ""
        }
    }

    return queries
}
