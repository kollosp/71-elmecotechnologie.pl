module.exports = function (ctx) {

    const Op = ctx.getOp()
    const db = ctx.getSequelize()
    const bcrypt = require('bcrypt')

    const functions = {
        findUserByEmail: async function({email}){

        },

        findUserById: async function({userId}){

        },

        findUserByUsername: async function({username}){

        },

        verifyUser: async function({username, password}){
            let user = await db.UM_USER.findOne({
                attributes: ['id'],
                include: [{
                    model: db.UM_LOGINDATA,
                    as: 'logindata',
                    attributes: ['password', 'username'],
                    where: {
                        username
                    }
                },{
                    model: db.UM_PERSONDATA,
                    as: 'persondata',
                    attributes: ['name', 'surname']
                }],
                raw:true,
                nest:true
            })

            if(user) {
                if ((await bcrypt.compare(password, user.logindata.password)) == true) {
                    //credentials ok
                    return user
                } else {
                    //credentials incorrect
                    return false
                }
            }
            else{
                //unknown username
                return null
            }

        },
    }

    return functions
}
