
module.exports = function(ctx) {
    return {
        insertHistory: async function({subject, content, receivers, status, statusDescription, date}) {
            let db = ctx.getSequelize()
            let notification = await db.UM_EMAILHISTORY.create({
                subject,
                content,
                receivers,
                status,
                statusDescription,
                date
            })

            return notification
        }
    }
}
