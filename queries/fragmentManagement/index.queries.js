
module.exports = function(ctx) {
    return {
        findFragment: async function({key}) {
            let db = ctx.getSequelize()
            let fragment = await db.FM_FRAGMENT.findOne({
                where: {
                    key
                },
                attributes: ['content']
            })

            //if given key do not defined in database
            if(fragment == null) return null

            let f = fragment.content.trim()
            f = f.replace('\n', '').replace('\r', '')

            return f
        },

        findAllFragments: async function() {
            let db = ctx.getSequelize()
            let fragments = await db.FM_FRAGMENT.findAll({
                attributes: ['key','content']
            })

            for(let i in fragments) {
                let f = fragments[i].content.trim()
                f = f.replace('\n', '').replace('\r', '')
                fragments[i].content = f
            }

            return fragments
        }
    }
}
