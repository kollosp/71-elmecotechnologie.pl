
module.exports = function(ctx) {
    return {
        findFile: async function({filename}) {
            let db = ctx.getSequelize()
            let page = await db.PM_IMAGE.findOne({
                where: {
                    name:filename
                },
                attributes: ['data', 'extension']
            })

            //if no page under given url
            if(page == null) return null

            return page.toJSON()
        },
        findFiles: async function() {
            let db = ctx.getSequelize()
            let page = await db.PM_IMAGE.findAll({
                attributes: ['name', 'extension']
            })

            //if no page under given url
            if(page == null) return null

            return page.map(value => {return value.toJSON()})
        },
        updateOrCreateFile: async function({fileName, data, extension, alt, isAutoCreated}){
            let db = ctx.getSequelize()
            let file = await db.PM_IMAGE.findOne({
                where: {
                    name:fileName
                },
                attributes: ['id']
            })

            if(file){
                await db.PM_IMAGE.update({
                    name:fileName,
                    data,
                    extension,
                    alt: alt,
                    isAutoCreated
                },{
                    where: {
                        id:file.id
                    }
                })

                return db.PM_IMAGE.findOne({
                    where: {
                        id:file.id
                    },
                    attributes: ['id']
                })
            }else{
                return await db.PM_IMAGE.create({
                    name:fileName,
                    data,
                    extension,
                    alt: alt
                })
            }
        }
    }
}
