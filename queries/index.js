/**
 * This is the main queries file. The file contains script which include / load every possible
 * query structure.
 */



const fs = require('fs')
const path = require('path')

const resolveFolder = function (currentPath, ctx) {
    //to do
    //add every index.endpoint.js file as a new router
    //in case of new directory go inside and then ...

    let obj = {}

    fs.readdirSync(currentPath).forEach(function(file) {
        //if endpoints
        if (file.match(/\.queries\.js$/) !== null) {
            let name = file.replace('.queries.js', '');

            //pass ctx to queries
            let queries = require(path.join(currentPath, file))(ctx)

            if(name == 'index'){
                //integrate queries inside a object because it is index object
                obj = {...obj, ...queries}
            }else{
                obj[name] = queries
            }

        }
        //if folder
        else if(fs.lstatSync(path.join(currentPath, file)).isDirectory()){
            obj[file] = resolveFolder(path.join(currentPath, file),ctx)
        }
    })

    return obj
}


module.exports = function (ctx) {
    return resolveFolder(__dirname, ctx);
}
