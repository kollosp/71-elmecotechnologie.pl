const {where} = require("sequelize");
module.exports = function (ctx) {

    const Op = ctx.getOp()
    const db = ctx.getSequelize()
    const views = require('./tableViews')(ctx)

    //constrains = [{'fieldName': value}] or [{'fieldname': {'operator': value}}]
    const rewriteConstrainOperator = function (constrains) {
        for(let cons of constrains){
            for(let field in cons){
                //if it is object not array
                if(typeof cons[field] === 'object' && !Array.isArray(cons[field])) {
                    let keys = Object.keys(cons[field])

                    //iterate throw all operators
                    for (let key of keys) {
                        let val = JSON.parse(JSON.stringify(cons[field][key]))
                        switch (key) {
                            case 'like':
                                delete cons[field][key]
                                cons[field][Op.like] = "%" + val + "%"
                                break;
                            case 'gte':
                                delete cons[field][key]
                                cons[field][Op.gte] = val
                                break;
                            case 'gt':
                                delete cons[field][key]
                                cons[field][Op.gt] = val
                                break;
                            case 'lte':
                                delete cons[field][key]
                                cons[field][Op.lte] = val
                                break;
                            case 'lt':
                                delete cons[field][key]
                                cons[field][Op.lt] = val
                                break;
                        }
                    }
                }
            }
        }
        return undefined
    }

    const prepareFilters = function(filter){
        let filters = []
        for(let f in filter){
            if (f == 'id') {
                filters.push({[f]: filter[f]})
            } else {
                //filters.push({['$' + f + '$']: {[Op.like]: `%${filter[f]}%`}})
                filters.push({[f]: {[Op.like]: `%${filter[f]}%`}})
            }
        }
        if(filters.length > 0){
            filters = {[Op.or] : filters}
        }
        return filters
    }

    const prepareWhereClauses = function(attributes, filter){
        let filters = []
        for(let f in filter){
            if (f == 'id') {
                filters.push({[f]: filter[f]})
            } else {

                let attribute = f.split('.')

                let includes = attributes

                while(attribute.length > 1){
                    for(let include of includes.include){
                        if(include.as == attribute[0]){
                            includes = include
                        }
                    }
                    attribute.shift()
                }

                if(includes.where && includes.where[Op.or]){
                    includes.where[Op.or].push({['$' + attribute[0] + '$']: {[Op.like]: `%${filter[f]}%`}})
                }else if(includes.where){
                    includes.where[Op.or] = [
                        {['$' + attribute[0] + '$']: {[Op.like]: `%${filter[f]}%`}}
                    ]
                }else{
                    includes.where = {
                        [Op.or]: [
                            {['$' + attribute[0] + '$']: {[Op.like]: `%${filter[f]}%`}}
                        ]
                    }
                }
            }
        }
        return attributes
    }


    const prepareThroughTable = async function(throughTable, foreignKey, constraints){
        let idFilter = await db[throughTable].findAll({
            where: constraints,
            attributes: [foreignKey]
        })
        return [{id: idFilter.map((val) => {return val[foreignKey]})}]
    }

    const prepareOptions = function (options){
        if(!options) options = {}
        return options
    }

    const getView = function({table, constraints, options, filter, throughTable, foreignKey}) {
        for(let view of views){
            if(view.name == table){
                return view
            }
        }

        return null
    }

    const findAllInView = async function(view, arguments){
        /**
         * if throughTable given (used to filter by many to many and many to one relations) then constraints
         * are changed
         */
        if(arguments.throughTable) {
            arguments.constraints = await prepareThroughTable(arguments.throughTable, arguments.foreignKey, arguments.constraints)
        }

        let rawNast = {}
        if(!view.nested){
            rawNast = {
                nested: false,
                raw: true,
            }
        }
        let attr = view.func(arguments)
        let attributes = Object.assign({},{
            where: Object.assign({}, ...arguments.constraints),
        }, rawNast, prepareOptions(arguments.options))

        prepareWhereClauses(attributes, arguments.filter)

        if(attr instanceof Function){
            return await attr(attributes)
        }else{

            let data = await db[view.model].findAll(Object.assign({}, attr, attributes))

            if(view.nested){
                data = data.map(val => {return val.toJSON()})
            }
            return data
        }
    }

    const countTableInView = async function(view, arguments){
        /**
         * if throughTable given (used to filter by many to many and many to one relations) then constraints
         * are changed
         */
        if(arguments.throughTable) {
            arguments.constraints = await prepareThroughTable(arguments.throughTable, arguments.foreignKey, arguments.constraints)
        }

        if(view.count){
            let d = await db[view.model].count(view.count(arguments))
            return d
        }

        let filter = prepareFilters(arguments.filter)
        let attr = view.func(arguments)


        if(attr.where)
            attr.where = {...attr.where, ...arguments.constraints, ...filter} //Object.assign({}, attr.where, )
        else
            attr.where = Object.assign({},...arguments.constraints,  ...filter)


        attr.nested = false
        attr.raw = true

        //console.log("attr", attr, prepareOptions(arguments.options))

        let attributes = Object.assign({}, attr, prepareOptions(arguments.options))

        prepareWhereClauses(attributes, arguments.filter)

        //attributes.attributes = []

        let d = await db[view.model].count(attributes)

        return d
    }

    const findAllInTable = async function({table, constraints, options, filter, throughTable, foreignKey, attributes}){
        /**
         * if throughTable given (used to filter by many to many and many to one relations) then constraints
         * are changed
         */
        if(throughTable) {
            constraints = await prepareThroughTable(throughTable, foreignKey, constraints)
        }

        filter = prepareFilters(filter)

        let attr = []
        if(attributes){
            attr = attributes
        }else{
            attr = ['id', ...Object.keys(db[table].tableAttributes)]
        }

        //console.log(Object.assign({}, ...constraints, filter))
        options = Object.assign({
            include: { all: ['BelongsTo'], nested: true },
            where: Object.assign({}, ...constraints, filter),

            //include id in association table
            attributes: attr
        }, prepareOptions(options))


        //console.log(JSON.stringify(options, null,4),constraints)
        let d = await db[table].findAll(options)
        //console.log(JSON.parse(JSON.stringify(d)))
        return d
    }


    const functions= {

        showTables: async function () {
            let ret = []
            for (let i in db) {
                if (i != 'Sequelize' && i != 'sequelize') {
                    ret.push(i)
                }
            }

            return ret
        },

        describeTable: async function (table, depth=1) {
            //console.log("=======================")
            //if view overwrite table name then send fields from definition of the view
            let view = getView({table})
            if(view){
                return {fields: view.fields, associations: []}
            }

            let d = await db.sequelize.getQueryInterface().describeTable({
                tableName: table
            })

            let associations = []
            //console.log(Object.keys(db[table].associations),db[table].associations )
            for(let key of Object.keys(db[table].associations)){
                let desc = {}
                let o = db[table].associations[key]


                //console.log("+", o.associationType)
                //console.log("+", db[table].associations[key].foreignKey)
                //console.log("- throughModel:", o.throughModel)

                //desc.target = o.target
                desc.associationType = o.associationType
                desc.as = o.as //use to tell sequelize how to associate models
                desc.foreignKey = o.foreignKey
                desc.target = o.target.tableName
                desc.source = o.source.tableName


                if(desc.associationType == 'BelongsToMany'){
                    desc.throughModel = o.throughModel.tableName
                    /*Object.keys(o).forEach((val) => {
                        console.log("---", val, o[val])
                    })*/
                    associations.push(desc)
                } else if(desc.associationType == 'HasMany'){
                    associations.push(desc)
                }

            }

            for(let key in d){
                try {
                    if (db[table].tableAttributes[key].references) {
                        d[key].references = {
                            ...db[table].tableAttributes[key].references,
                        }
                        //console.log(Object.keys(db[table].tableAttributes[key]), )
                        //try to match associations table to foreign key
                        for(let i in db[table].associations){
                            if(db[table].tableAttributes[key].field == db[table].associations[i].foreignKey){
                                d[key].references.as = db[table].associations[i].as
                            }
                            //console.log(i, Object.keys(db[table].associations[i]), "as", , )
                        }

                        //recurrence limit. max depth = 2. Only root and first child
                        if(depth < 2){
                            d[key].references = {
                                ...d[key].references,
                                details: await functions.describeTable(d[key].references.model, depth+1)
                            }
                            //remove unnecessary fields
                            //delete d[key].references.details.createdAt
                            //delete d[key].references.details.updatedAt
                        }
                    }
                }catch (e) {

                }
            }
            //console.log(d)
            d = {
                fields: d,
                associations: associations
            }
            //console.log(associations)
            //console.log("=======================")

            return d
        },

        findAll: async function ({table, constraints, options, filter, throughTable, foreignKey, attributes}) {
            let arguments = {table, constraints, options, filter, throughTable, foreignKey, attributes}
            let view = getView(arguments)

            if(view){
                return await findAllInView(view, arguments)
            }else{
                return await findAllInTable(arguments)
            }
        },

        findAllAssociated: async function ({table, associated, as, options}) {
            if(!options) options = {}

            options = Object.assign({
                include: [{
                    model: db[associated],
                    as: as
                }]
            }, options)

            let d = await db[table].findAll(options)

            return d
        },

        findOne: function ({table, id, options}) {
            let view = getView({table})
            let model = table
            if(view){
                model = view.model
                return db[model].findOne(Object.assign({
                    where: {
                        id: id,
                    }
                }, view.func(arguments)))
            }else{
                return db[model].findOne(Object.assign({
                    where: {
                        id: id,
                    },
                    include: { all: ['BelongsTo'], nested: true },
                }, options))
            }

        },

        countTable: async function ({table, constraints, filter, throughTable, foreignKey}) {

            let count = []
            let view = getView({table})
            if(view){
                //console.log(" countTableInView")
                return {count: await countTableInView(view,{table, constraints, filter, throughTable, foreignKey})}
            }else{
                /**
                 * if throughTable given (used to filter by many to many and many to one relations) then constraints
                 * are changed
                 */
                if(throughTable) {
                    constraints = await prepareThroughTable(throughTable, foreignKey, constraints)
                }

                filter = prepareFilters(filter)
                //console.log("filter", filter )

                console.log(table, Object.assign({}, ...constraints, filter))
                count = await db[table].findAll({
                    attributes: [[db.sequelize.fn('COUNT', db.sequelize.col('id')), 'count']],
                    //where: Object.assign({}, constraints, {[Op.or] : filter})
                    where: Object.assign({}, ...constraints, filter)
                })

                if(count.length > 0)
                    return count[0]
                else
                    return 0
            }
        },

        updateOrInsert: async function ({table, id, data}) {
            for(let i in data){
                if(data[i] == 'null') data[i] = null
            }

            let view = getView({table})
            if(view){
                if(view.updateOrInsert){
                    return await view.updateOrInsert({id, data})
                }else{
                    throw (new Error(`Model ${table} is a view model and does not provide updateOrInsert method!`))
                }
            }else{
                let description = await functions.describeTable(table, 1)

                //change base64 in blob fields into blob data
                for(let field in description.fields) {
                    if (description.fields[field].type.match(/.*BLOB.*/)) {
                        if (data[field]) {
                            data[field] = new Buffer(data[field], 'base64')
                        }
                    }
                }

                let obj = await db[table].findOne({
                    where: {
                        id
                    }
                }).then()

                if(obj){
                    return await db[table].update(data, {
                        where: {
                            id: id,
                        }
                    })
                }else{
                    return await db[table].create(data)
                }
            }
        },

        destroyRow: async function ({table, id}) {
            let view = getView({table})
            let model = table
            if(view){
                model = view.model
            }

            let d = await db[model].destroy({
                where: {
                    id: id
                }
            })

            return {removedItems: d}
        },

        description : async function({table, id}) {
            let view = getView({table})
            let model = table
            if(view){
                model = view.model
            }

            let d = await db[model].findOne({
                where: {
                    id: id
                },
                attributes: ['desc']
            })

            return d
        },

        name : async function({table, id}) {
            let view = getView({table})
            let model = table
            if(view){
                model = view.model
            }

            let d = await db[model].findOne({
                where: {
                    id: id
                },
                attributes: ['id', 'name']
            })

            return d
        },
        name : async function({table, id}) {
            let view = getView({table})
            let model = table
            if(view){
                model = view.model
            }

            let d = await db[model].findOne({
                where: {
                    id: id
                },
                attributes: ['id', 'name']
            })

            return d
        },
    }

    return functions
}
