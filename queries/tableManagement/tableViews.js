const bcrypt = require("bcrypt");

function array_move(arr, old_index, new_index) {
    while (old_index < 0) {
        old_index += arr.length;
    }
    while (new_index < 0) {
        new_index += arr.length;
    }
    if (new_index >= arr.length) {
        let k = new_index - arr.length + 1;
        while (k--) {
            arr.push(undefined);
        }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr; // for testing purposes
};

//constrains = [{'fieldName': value}] or [{'fieldname': {'operator': value}}]
const extractConstrains = function (constrains, fieldName) {
    for(let cons of constrains){
        for(let field in cons){
            if(field == fieldName){
                return cons[field]
            }
        }
    }
    return undefined
}

const stringLikeOverlap = function (str){
    if(str){
        return "%" + str + "%"
    }
    else{
        return "%"
    }
}

module.exports = function (ctx) {
    const db = ctx.getSequelize()
    return [{
        name: 'PM_PRODUCT_QR',
        model: 'PM_PRODUCT',
        foreignKey: null,
        nested: true,
        func: function ({table, constraints, options, filter, throughTable, foreignKey}) {
            return {
                attributes: ['id', 'description', 'shortDesc'],
                include: [{
                    model: db.PM_MANUFACTURER,
                    as: 'manufacturer',
                    attributes: ['id', 'name']
                }, {
                    model: db.PM_IMAGE,
                    as: 'qrImage',
                    attributes: ['name']
                }, {
                    model: db.PM_IMAGE,
                    as: 'images',
                    attributes: ['name']
                }, {
                    model: db.PM_PRODUCTTYPE,
                    as: 'productType',
                    attributes: ['name']
                }],
                order: ['shortDesc']
            }
        },
        fields: [
            {name: "id", type: "INT"},
            {name: "persondata.name", type: "VARCHAR(255)"},
            {name: "persondata.surname", type: "VARCHAR(255)"},
            {name: "persondata.phone", type: "VARCHAR(255)"},
            {name: "logindata.username", type: "VARCHAR(255)"}
        ]
    },{
        name: 'PM_PRODUCTATTRIBUTE_ORDERED',
        model: 'PM_PRODUCTATTRIBUTE',
        foreignKey: null,
        nested: true,
        func: function ({table, constraints, options, filter, throughTable, foreignKey}) {
            return {
                attributes: ['id', 'yIndex', 'name', 'value'],
                order: ['yIndex']
            }
        },
        fields: []
    },{
        name: 'PM_IMAGE_NAME',
        model: 'PM_IMAGE',
        foreignKey: null,
        nested: true,
        func: function ({table, constraints, options, filter, throughTable, foreignKey}) {
            return {
                attributes: ['id', 'name'],
                where: {
                    isAutoCreated: false
                }
            }
        },
        fields: []

    },{
        name: 'PM_PRODUCT_NAME',
        model: 'PM_PRODUCT',
        foreignKey: null,
        nested: true,
        func: function ({table, constraints, options, filter, throughTable, foreignKey}) {
            return {
                attributes: ['id', ['shortDesc','name']],
                order: ['shortDesc']
            }
        },
        fields: []
    },{
        name: 'PM_PRODUCTIMAGE',
        model: 'PM_PRODUCTIMAGE',
        foreignKey: null,
        nested: true,
        updateOrInsert: async function ({id, data}){
            let obj = await db.PM_PRODUCTIMAGE.findOne({
                where: {
                    id
                }
            })

            let ret = id
            if(obj){
                await db.PM_PRODUCTIMAGE.update(data, {
                    where: {
                        id
                    }
                })
            }else{

                let d = await db.PM_PRODUCTIMAGE.create(data)
                ret = d.id
            }

            return ret
        },
        func: function ({table, constraints, options, filter, throughTable, foreignKey}) {
            return {
                attributes: ['id', 'imageId', 'productId'],
                include: [{
                    model: db.PM_IMAGE,
                    as: 'image',
                    attributes: ['id', 'extension', 'name'],
                },{
                    model: db.PM_PRODUCT,
                    as: 'product',
                    attributes: ['id']
                }]
            }
        },
        fields: []
    },{
        name: 'PM_DELIVERY_PREVIEW',
        model: 'PM_DELIVERY',
        foreignKey: null,
        nested: true,
        func: function (attr) {
            return {
                attributes: ['id', 'note', 'name', 'deliveryTypeId', 'date', 'userId', 'deliveryDirection'],
                order: [['date', 'DESC']],

            }
        },
        fields: []
    },{
        name: 'UM_USER_ID',
        model: 'UM_USER',
        foreignKey: null,
        nested: true,
        func: function ({table, constraints, options, filter, throughTable, foreignKey}) {
            const { fn, col } = ctx.getSequelize().Sequelize;
            return {
                attributes: ['id', [fn('concat', col('persondata.name'), ' ', col('persondata.surname')),'name']],
                include: [{
                    model: db.UM_PERSONDATA,
                    as: 'persondata',
                    attributes: []
                }],
            }
        },
        fields: []
    },{
        name: 'PM_PRODUCTDELIVERY_PREVIEW',
        model: 'PM_PRODUCTDELIVERY',
        foreignKey: null,
        nested: true,
        func: function ({table, constraints, options, filter, throughTable, foreignKey}) {
            const { fn, col } = ctx.getSequelize().Sequelize;
            return {
                attributes: ['id', 'productId',[col('product.shortDesc'), 'productName'],  'productCount', 'productPrice'],
                include: [{
                    model: db.PM_PRODUCT,
                    as: 'product',
                }],
                order: [col('product.shortDesc')],
            }
        },
        fields: []
    },{
        name: 'PM_PRODUCTDELIVERY2_PREVIEW',
        model: 'PM_PRODUCTDELIVERY',
        foreignKey: null,
        nested: true,
        func: function ({table, constraints, options, filter, throughTable, foreignKey}) {
            const { fn, col } = ctx.getSequelize().Sequelize;
            let price = extractConstrains(constraints, 'productPrice')
            return {
                attributes: ['id', 'deliveryId',[col('delivery.name'), 'deliveryName'],[col('delivery.note'), 'deliveryNote'],
                    [col('delivery.deliveryDirection'), 'deliveryDirection'], [col('delivery.date'), 'deliveryDate'],'productCount', 'productPrice'],
                include: [{
                    model: db.PM_DELIVERY,
                    as: 'delivery',
                }],
                where: {
                    productPrice: price
                },
                order: [[col('delivery.date'), "DESC"]],
            }
        },
        fields: []
    },{
        name: 'PM_PRODUCT_PREVIEW',
        model: 'PM_PRODUCTDELIVERY',
        foreignKey: null,
        nested: true,
        count: function (attr){
            const { Op, fn, col, literal} = ctx.getSequelize().Sequelize;

            //console.log(attr)
            attr.filter = [attr.filter]
            let date = new Date(extractConstrains(attr.filter, 'date'))
            let shortDesc = stringLikeOverlap(extractConstrains(attr.filter, 'shortDesc'))
            let description = stringLikeOverlap(extractConstrains(attr.filter, 'description'))
            let warehouse = extractConstrains(attr.constraints, 'warehouseId')

            attr.filter = {}
            attr.constraints = {}
            return {
                attributes: ['productId', 'productPrice', [fn('sum', col('productCount')), 'pCount']],
                having: {
                    pCount: {
                        [Op.not]: 0
                    }
                },
                include: [{
                    model: db.PM_PRODUCT,
                    as: 'product',
                    attributes: [],
                    where: {
                        shortDesc: {
                            [Op.like]: shortDesc,
                        },
                        description: {
                            [Op.like]: description,
                        }
                    },
                },{
                    model: db.PM_DELIVERY,
                    as: 'delivery',
                    attributes: [],
                    where: {
                        date: {
                            [Op.lte]: date
                        },
                        deliveryTypeId: 2, //delivered,
                        warehouseId: warehouse
                    }
                }],
                order: [col('product.shortDesc')],
                group:['productId',literal('cast(productPrice as decimal(10,2))')]
            }
        },
        func: function (attr) {
            const { Op, fn, col, literal} = ctx.getSequelize().Sequelize;

            //extract expected arguments from constrains.
            //console.log(attr)
            attr.filter = [attr.filter]
            let date = new Date(extractConstrains(attr.filter, 'date'))
            let shortDesc = stringLikeOverlap(extractConstrains(attr.filter, 'shortDesc'))
            let description = stringLikeOverlap(extractConstrains(attr.filter, 'description'))
            let warehouse = extractConstrains(attr.constraints, 'warehouseId')
            //remove unnecessary or unexpected constraints form query
            attr.constraints = []
            attr.filter = []

            return {
                attributes: ['productId', 'productPrice', [fn('sum', col('productCount')), 'pCount'],
                    [col('product.shortDesc'), 'shortDesc'], [col('product.description'), 'description'],
                    [fn('max', col('delivery.date')), 'date']],
                having: {
                    pCount: {
                        [Op.not]: 0
                    }
                },
                include: [{
                    model: db.PM_PRODUCT,
                    as: 'product',
                    attributes: [],
                    where: {
                        shortDesc: {
                            [Op.like]: shortDesc,
                        },
                        description: {
                            [Op.like]: description,
                        }
                    },
                },{
                    model: db.PM_DELIVERY,
                    as: 'delivery',
                    attributes: [],
                    where: {
                        date: {
                            [Op.lte]: date
                        },
                        deliveryTypeId: 2, //delivered
                        warehouseId: warehouse,
                    }
                }],
                order: [col('product.shortDesc')],
                group:['productId', literal('cast(productPrice as decimal(10,2))')]
            }
        },
        fields: []
    },{
        name: 'PM_PRODUCT_AVAILABLE',
        model: 'PM_PRODUCTDELIVERY',
        foreignKey: null,
        nested: true,
        updateOrInsert: async function ({id, data}){
            const {Op, fn, col, literal} = ctx.getSequelize().Sequelize;
            const transaction = await ctx.getSequelize().sequelize.transaction();
            try {
                let delivery = await db.PM_DELIVERY.findOne({attributes: ['warehouseId'], where: {
                    id:data.deliveryId
                }})
                let warehouseId = delivery.warehouseId

                let where = {
                    productId: data.productId
                }

                let obj = await db.PM_PRODUCTDELIVERY.findAll({
                    attributes: [[fn('sum', col('productCount')), 'productCount'], 'productPrice'],
                    include: [{
                        model: db.PM_DELIVERY,
                        as: 'delivery',
                        attributes: [],
                        where: {
                            warehouseId
                        }
                    }],
                    where: where,
                    having: {
                        productCount: {
                            [Op.gt]: 0
                        }
                    },
                    group: [literal('cast(productPrice as decimal(10,2))')],
                    order: [col('delivery.date')],
                    raw: true
                }, {transaction})

                //if price is given inside form then take the price as a first price to consider
                if(data.productPrice){
                    let index = 0
                    for(let i in obj){
                        if (obj[i].productPrice == data.productPrice)
                            index = i
                    }

                    if(index != 0){
                        obj = array_move(obj, index, 0)
                    }
                }

                console.log(data.productPrice, obj)

                let toPickup = {productPrice: 0, productCount: parseFloat(data.productCount)}
                let pickUp = []
                for(let i in obj){
                    if(toPickup.productCount > 0){
                        //difference beetween stock and request
                        let diff = obj[i].productCount - toPickup.productCount
                        //where is more? in reference or in stock
                        let productCount = diff > 0 ? toPickup.productCount : obj[i].productCount
                        pickUp.push({
                            productPrice: obj[i].productPrice,
                            productCount
                        })
                        //some products picked up decrease count to be picked
                        toPickup.productCount -= productCount
                    }
                }
                //if more products taken then available in stoke. Then there are still product to be taken.
                //take them with price 0
                if(toPickup.productCount > 0){
                    pickUp.push(toPickup)
                }

                for(let i in pickUp){
                    await db.PM_PRODUCTDELIVERY.create({
                        productPrice: pickUp[i].productPrice,
                        productCount: -pickUp[i].productCount,

                        deliveryId: data.deliveryId,
                        productId: data.productId,
                    })
                }

                await transaction.commit()
            }catch (e){
                console.error(e)
                await transaction.rollback()
            }

            return []
        },
        func: function (attr) {
            const {fn, col, literal} = ctx.getSequelize().Sequelize;
            let warehouseId = extractConstrains(attr.constraints, 'warehouseId')
            let productId = extractConstrains(attr.constraints, 'productId')
            let productPrice = extractConstrains(attr.constraints, 'productPrice')
            attr.constraints = []

            if(productPrice){
                return {
                    attributes: ['productId', [fn('sum', col('productCount')), 'available'], 'productPrice'],
                    having: literal(`cast(100*productPrice as decimal(10,0)) = '${Math.floor(productPrice *100)}'`),
                    include: [{
                        model: db.PM_PRODUCT,
                        as: 'product',
                        attributes: [],
                        where: {
                            id: productId
                        }
                    },{
                        model: db.PM_DELIVERY,
                        as: "delivery",
                        where: {
                            warehouseId
                        }
                    }],
                    order: [col('product.shortDesc')],
                    group:['productId',literal('cast(productPrice as decimal(10,2))')]
                }
            }else{
                return {
                    attributes: ['productId', [fn('sum', col('productCount')), 'available']],
                    include: [{
                        model: db.PM_PRODUCT,
                        as: 'product',
                        attributes: [],
                        where: {
                            id: productId
                        }
                    },{
                        model: db.PM_DELIVERY,
                        as: "delivery",
                        where: {
                            warehouseId
                        }
                    }],
                    order: [col('product.shortDesc')],
                    group:['productId']
                }
            }

        },
        fields: []
    },{
        name: 'UM_USER_PREVIEW',
        model: 'UM_USER',
        foreignKey: null,
        nested: true,
        updateOrInsert: async function ({id, data}){
            const {Op, fn, col } = ctx.getSequelize().Sequelize;
            const transaction = await ctx.getSequelize().sequelize.transaction();
            let user
            try {
                user = await db.UM_USER.findOne({
                    where: {
                        id
                    },
                    attributes: ['id', 'personDataId', 'emailId', 'loginDataId'],
                    raw: true
                }, {transaction})

                //update user
                if (user) {
                    let email = await db.UM_EMAIL.update({
                        email: data.emailAddress
                    },{where: {id: user.emailId}}, {transaction})
                    let persondata = await db.UM_PERSONDATA.update({
                        name: data.name,
                        surname: data.surname,
                        phone: data.phone,
                    },{where: {id: user.personDataId}}, {transaction})
                    let logindata = await db.UM_LOGINDATA.update({
                        username: data.username
                    },{where: {id: user.loginDataId}}, {transaction})

                    user = await db.UM_USER.update({
                        deleted: data.deleted
                    },{where: {id: user.id}}, {transaction})
                }
                //create new user
                else {
                    let email = await db.UM_EMAIL.create({
                        email: data.emailAddress
                    }, {transaction})
                    let persondata = await db.UM_PERSONDATA.create({
                        name: data.name,
                        surname: data.surname,
                        phone: data.phone,
                    }, {transaction})
                    let logindata = await db.UM_LOGINDATA.create({
                        username: data.username
                    }, {transaction})

                    user = await db.UM_USER.create({
                        personDataId: persondata.id,
                        loginDataId: logindata.id,
                        emailId: email.id,
                        deleted: data.deleted
                    }, {transaction})
                }
                transaction.commit()
            }catch (e){
                transaction.rollback()
                throw e
            }

            return user
        },
        func: function ({table, constraints, options, filter, throughTable, foreignKey}) {
            const { Op, fn, col } = ctx.getSequelize().Sequelize;
            return {
                attributes: ['id', 'deleted', 'personDataId', 'loginDataId', 'emailId',
                    [col('persondata.name'), 'name'], [col('persondata.surname'), 'surname'],[col('persondata.phone'), 'phone'],
                    [col('email.email'), "emailAddress"],
                    [col('logindata.username'), 'username'], [col('logindata.lastLogin'), 'lastLogin'],
                    ],
                include: [{
                    model: db.UM_PERSONDATA,
                    as: 'persondata',
                    attributes: []
                },{
                    model: db.UM_EMAIL,
                    as: 'email',
                    attributes: []
                },{
                    model: db.UM_LOGINDATA,
                    as: 'logindata',
                    attributes: []
                }],
            }
        }
    },{
        name: 'UM_USER_PASSWORD',
        model: 'UM_LOGINDATA',
        foreignKey: null,
        nested: true,
        updateOrInsert: async function ({id, data}){
            const bcrypt = require('bcrypt')

            let logindata = await db.UM_LOGINDATA.findOne({where: {id}})
            if(logindata){
                db.UM_LOGINDATA.update({
                    password: await bcrypt.hash(data.password,10)
                },{where: {id}})
            }else{
                db.UM_LOGINDATA.create({
                    password: await bcrypt.hash(data.password,10)
                })
            }
        },
        func: function ({table, constraints, options, filter, throughTable, foreignKey}) {
            const { Op, fn, col } = ctx.getSequelize().Sequelize;
            return {
                attributes: ['id', 'password', 'username']
            }
        },
        fields: []
    },{
        name: 'HP_HEATPUMP_OVERVIEW',
        model: 'HP_HEATPUMP',
        foreignKey: null,
        nested: true,
        func: function ({table, constraints, options, filter, throughTable, foreignKey}) {
            const { fn, col } = ctx.getSequelize().Sequelize;
            return {
                attributes: ['id', 'name', 'notes', 'mountedAt', 'refrigerantId',[col('refrigerant.name'), 'refrigerantName'],
                    [col('refrigerant.gwp'), 'refrigerantGWP'], 'refrigerantWeight',
                    [col('product.shortDesc'), 'productShortDesc'],
                    [col('product.id'), 'productId'], 'customerId',
                    [fn('concat', col('customer.name'), ' ', col('customer.surname')), 'customerName'],
                    [col('customer.phone'), 'customerPhone'], 'notificationPolicyId'],
                include: [{
                    model: db.HP_REFRIGERANT,
                    as: 'refrigerant',
                },{
                    model: db.PM_PRODUCT,
                    as: 'product',
                },{
                    model: db.HP_CUSTOMER,
                    as: 'customer',
                }],
                order: [],
            }
        },
        fields: []
    },{
        name: 'HP_HEATPUMP_COMING_EVENTS',
        model: 'HP_HEATPUMP',
        foreignKey: null,
        nested: true,
        func: function (attr) {
            const { Op, fn, col,literal } = ctx.getSequelize().Sequelize;

            //extract expected arguments from constrains.
            //console.log(attr)
            attr.filter = [attr.filter]
            // let date = new Date(extractConstrains(attr.filter, 'date'))
            // let shortDesc = stringLikeOverlap(extractConstrains(attr.filter, 'shortDesc'))
            // let description = stringLikeOverlap(extractConstrains(attr.filter, 'description'))
            // let warehouse = extractConstrains(attr.constraints, 'warehouseId')
            // //remove unnecessary or unexpected constraints form query
            attr.constraints = []
            attr.filter = []
            return {
                subQuery: false,
                attributes: ['id',
                    [fn('max', col('services.date')), 'lastService'],
                    'name',
                    'mountedAt',
                    [col('refrigerant.gwp'), 'refrigerantGWP'],
                    [col('refrigerant.name'), 'refrigerantName'],
                    'refrigerantWeight',
                    'serviceNotificationAllowed',
                    'customerNotificationAllowed',
                    [col('notificationpolicy.name'), 'notificationpolicyName'],
                    [col('notificationpolicy.period'), 'period'],
                    [literal('DATE_ADD(MAX(services.date), INTERVAL period MONTH)'), 'nextService'],
                    [literal('DATE_ADD(DATE_ADD(MAX(services.date), INTERVAL period MONTH), INTERVAL -notificationpolicy.firstNotification DAY)'), 'firstNotification'],
                    [literal('DATE_ADD(DATE_ADD(MAX(services.date), INTERVAL period MONTH), INTERVAL -notificationpolicy.secondNotification DAY)'), 'secondNotification'],
                    [literal('DATE_ADD(DATE_ADD(MAX(services.date), INTERVAL period MONTH), INTERVAL -notificationpolicy.thirdNotification DAY)'), 'thirdNotification'],
                    [literal("(SELECT count(id) FROM HP_NOTIFICATION as notifications WHERE notifications.fragmentNotificationId = 3 AND notifications.heatpumpId = `HP_HEATPUMP`.`id` AND"+
                        " notifications.date >= DATE_ADD(DATE_ADD(MAX(services.date), INTERVAL period MONTH), INTERVAL -notificationpolicy.firstNotification DAY) " +
                        " AND notifications.date <= DATE_ADD(DATE_ADD(MAX(services.date), INTERVAL period MONTH), INTERVAL -notificationpolicy.secondNotification DAY))"), "firstSend"],
                    [literal("(SELECT count(id) FROM HP_NOTIFICATION as notifications WHERE notifications.fragmentNotificationId = 3 AND notifications.heatpumpId = `HP_HEATPUMP`.`id` AND"+
                        " notifications.date >= DATE_ADD(DATE_ADD(MAX(services.date), INTERVAL period MONTH), INTERVAL -notificationpolicy.secondNotification DAY) " +
                        " AND notifications.date <= DATE_ADD(DATE_ADD(MAX(services.date), INTERVAL period MONTH), INTERVAL -notificationpolicy.thirdNotification DAY))"), "secondSend"],
                    [literal("(SELECT count(id) FROM HP_NOTIFICATION as notifications WHERE notifications.fragmentNotificationId = 3 AND notifications.heatpumpId = `HP_HEATPUMP`.`id` AND"+
                        " notifications.date >= DATE_ADD(DATE_ADD(MAX(services.date), INTERVAL period MONTH), INTERVAL -notificationpolicy.thirdNotification DAY) " +
                        " AND notifications.date <= DATE_ADD(MAX(services.date), INTERVAL period MONTH))"), "thirdSend"],
                    //time elapsed
                    [literal("(SELECT count(id) FROM HP_NOTIFICATION as notifications WHERE notifications.fragmentNotificationId = 4 AND notifications.heatpumpId = `HP_HEATPUMP`.`id` AND"+
                    " notifications.date >= DATE_ADD(MAX(services.date), INTERVAL period MONTH))"), "sendTimeElapsed"],
                    //welcome notification send
                    [literal("(SELECT count(id) FROM HP_NOTIFICATION as notifications WHERE (notifications.fragmentNotificationId = 2 OR notifications.fragmentNotificationId = 1) AND notifications.heatpumpId = `HP_HEATPUMP`.`id` AND"+
                        " notifications.date >= mountedAt)"), "welcomeSend"],
                    //service done notification send
                    [literal("(SELECT count(id) FROM HP_NOTIFICATION as notifications WHERE (notifications.fragmentNotificationId = 5 OR notifications.fragmentNotificationId = 6) AND notifications.heatpumpId = `HP_HEATPUMP`.`id` AND"+
                        " notifications.date >= max(`services`.`date`))"), "serviceDoneSend"],
                ],
                having: {
                    [Op.or]: [
                        {serviceNotificationAllowed: 1},
                        {customerNotificationAllowed: 1}
                    ]
                },
                include: [{
                    model: db.HP_REFRIGERANT,
                    as: 'refrigerant',
                },{
                    model: db.HP_HEATPUMPSERVICE,
                    as: 'services',
                    attributes: [],
                    required: false,
                },{
                    model: db.HP_NOTIFICATIONPOLICY,
                    as: 'notificationpolicy',
                    attributes: []
                }],
                group: ['HP_HEATPUMP.name'],
                order: [[literal('nextService'), 'ASC']]
            }

        },
        fields: []
    },{
        name: 'HP_NOTIFICATION_LIST',
        model: 'HP_NOTIFICATION',
        foreignKey: null,
        nested: true,
        func: function (attr) {
            const { Op, fn, col,literal } = ctx.getSequelize().Sequelize;

            return {
                attributes: ['heatpumpId', 'emailhistoryId', 'fragmentNotificationId',
                    //[col('emailhistory.date'), 'date'],
                    'date',
                    [col('fragmentnotification.name'), 'name']],
                include: [{
                    model: db.UM_EMAILHISTORY,
                    as: 'emailhistory',
                    attributes: ['date'],
                },{
                    model: db.HP_FRAGMENTNOTIFICATION,
                    as: 'fragmentnotification',
                    attributes: ['name'],
                }],
                order: [[col('date'), 'ASC']]
                //order: [[col('emailhistory.date'), 'ASC']]
            }

        },
        fields: []
    }]
}
