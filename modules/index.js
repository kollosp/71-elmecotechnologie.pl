/**
 * This is the main modules file. The file contains script which include / load every possible
 * module. Module is a class, so the file creates a new instance of every class.
 */

const fs = require('fs')
const path = require('path')

module.exports = function (ctx) {

    let modules = {}

    try {
        const files = fs.readdirSync(__dirname);
        for (const file of files){

            if(fs.lstatSync(path.join(__dirname, file)).isDirectory()){

                const moduleClass = require(path.join(__dirname, file, "index.module"))
                modules[file] = new moduleClass(ctx)
            }
        }
    } catch (err) {
        console.error(err)
    }

    return modules
}
