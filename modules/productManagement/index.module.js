const ModuleArchitecture = require('../ModuleArchitecture')

module.exports = class ProductManagement extends ModuleArchitecture {
    constructor(ctx) {
        super(ctx)

        this._private = {
        }
    }

    /**
     * Load from database specified fragment
     * @param fragmentKey
     */
    async loadSubtypesOf(typeName){
        if(typeName)
            return this.getCtx().getQueries().productManagement.findSubtypesOf({typeName})
        else
            return this.getCtx().getQueries().productManagement.findRootTypes()
    }

    async loadProductsOfType(typeName){
        return this.getCtx().getQueries().productManagement.findProductsOfType({typeName})
    }

    async loadProduct(productName) {
        return this.getCtx().getQueries().productManagement.findProduct({productName})
    }
}
