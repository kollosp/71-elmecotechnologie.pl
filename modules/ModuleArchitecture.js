module.exports = class ModuleArchitecture {
    constructor(ctx) {
        this.ModuleArchitecture = {
            ctx: ctx
        }
    }

    getCtx() {
        return this.ModuleArchitecture.ctx
    }
}
