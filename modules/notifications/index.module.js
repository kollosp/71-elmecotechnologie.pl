const ModuleArchitecture = require('../ModuleArchitecture')
const pdf = require("pdf-creator-node");
const fs = require('fs');
const path = require('path');
const ejs = require('ejs')

module.exports = class ProductManagement extends ModuleArchitecture {
    constructor(ctx) {
        super(ctx)

        this._private = {
        }
    }

    middleware () {
        let self = this
        return function (req, res, next) {

        }
    }

    renderOnlyDate(date) {
        date = new Date(date)
        return date.getDate().toString().padStart(2, '0') + "." + (date.getMonth() +1).toString().padStart(2, '0') + "." +
            date.getFullYear().toString().padStart(2, '0')
    }

    async notify({heatpumpId, notificationTypeId, text, subject,date, isTestMessage= true, }){
        if (!date) date = new Date()
        else date = new Date(date)

        let heatpump = await this.getCtx().getQueries().tableManagement.findOne({table:'HP_HEATPUMP_COMING_EVENTS',
            id: heatpumpId})

        let hp_installation = await this.getCtx().getQueries().tableManagement.findOne({table:'HP_HEATPUMP',
            id: heatpumpId})

        if (!text){
            text = await this.getCtx().getQueries().tableManagement.findOne({table: "HP_FRAGMENTNOTIFICATION", id:notificationTypeId})
            subject = text.fragment.key
            text = text.fragment.content
        }

        hp_installation = hp_installation.toJSON()
        let customer = hp_installation.customer
        heatpump = heatpump.toJSON()

        let emails = []
        let bcc = []
        //skip customer emails if it is test message
        if(isTestMessage == false){
            emails.push(customer.email)
            if(customer.altEmail != ""){
                emails.push(customer.altEmail)
            }

            bcc.push("biuro@elmecosolar.pl")
            bcc.push("pparczyk@elmecosolar.pl")

        }else{
            //emails.push("biuro@elmecosolar.pl")
            emails.push("pparczyk@elmecosolar.pl")
        }

        let contentData = {
            refrigerantName: heatpump.refrigerantName,
            refrigerantGWP: heatpump.refrigerantGWP,
            refrigerantWeight: heatpump.refrigerantWeight,
            co2: heatpump.refrigerantGWP*heatpump.refrigerantWeight,
            mountedAt: this.renderOnlyDate(heatpump.mountedAt),
            lastServiceAt: this.renderOnlyDate(heatpump.lastService),
            nextServiceAt: this.renderOnlyDate(heatpump.nextService),
        }

        try {
            text = ejs.render(text, contentData)
        }catch (e){
            console.error(e)
        }

        let email = await this.getCtx().getModule("mailer").sendEmail({
            replyTo: "biuro@elmecosolar.pl",
            to: emails.join(","),
            subject: subject,
            html: text,
            bcc:  bcc.join(","),
        })

        return this.getCtx().getQueries().notificationManagement.insertHistory({
            heatpumpId:heatpumpId,
            fragmentNotificationId: notificationTypeId,
            emailhistoryId: email.emailHistory.id,
            date: date
        })
    }

    async generateNotificationsList({date}){
        if (!date) date = new Date()
        else date = new Date(date)

        let list = await this.getCtx().getQueries().tableManagement.findAll({table:'HP_HEATPUMP_COMING_EVENTS'})
        let ret = []
        for (let listItem of list) {
            let isBDO = (listItem.refrigerantWeight * listItem.refrigerantGWP) > 5000
            let nextServiceAt = new Date(listItem.nextService)
            let firstNotificationAt = new Date(listItem.firstNotification)
            let secondNotificationAt = new Date(listItem.secondNotification)
            let thirdNotificationAt = new Date(listItem.thirdNotification)
            let lastServiceAt = new Date(listItem.lastService)
            let mountedAt = new Date(listItem.mountedAt)

            //hello message (after installation done)
            if(date > mountedAt && listItem.welcomeSend == false){
                ret.push({
                    heatpumpId: listItem.id,
                    notificationTypeId: isBDO ? 2 : 1,
                    txt: "hello message"
                })
            }

            //after service message
            if(date > lastServiceAt && listItem.serviceDoneSend == false){
                ret.push({
                    heatpumpId: listItem.id,
                    notificationTypeId: isBDO ? 6 : 5,
                    txt: "after service message"
                })
            }

            //first notification
            if(date > firstNotificationAt && date < secondNotificationAt && listItem.firstSend == false){
                ret.push({
                    heatpumpId: listItem.id,
                    notificationTypeId: 3,
                    txt: "first notification"
                })
            }

            //second notification
            if(date > secondNotificationAt && date < thirdNotificationAt && listItem.secondSend == false){
                ret.push({
                    heatpumpId: listItem.id,
                    notificationTypeId: 3,
                    txt: "second notification"
                })
            }

            //third notification
            if(date > thirdNotificationAt && date < nextServiceAt && listItem.thirdSend == false){
                ret.push({
                    heatpumpId: listItem.id,
                    notificationTypeId: 3,
                    txt: "third notification"
                })
            }

            //time elapsed notification
            if(date > nextServiceAt && listItem.sendTimeElapsed == false){
                ret.push({
                    heatpumpId: listItem.id,
                    notificationTypeId: 4,
                    txt: "time elapsed notification"
                })
            }
        }

        return ret
    }
}
