const ModuleArchitecture = require('../ModuleArchitecture')
const pdf = require("pdf-creator-node");
const fs = require('fs');
const path = require('path');


module.exports = class ProductManagement extends ModuleArchitecture {
    constructor(ctx) {
        super(ctx)

        this._private = {
        }
    }

    middleware () {
        let self = this
        return function (req, res, next) {

        }
    }

    generateTableStatement (header, data, documentParams) {
        return new Promise((resolve, reject) => {
            //let html = fs.readFileSync(path.join(__dirname, 'templates/agreement.html'), 'utf8');

            let options = {
                format: documentTemplate.format,
                orientation: "portrait",
                border: documentTemplate.border + 'mm',
                header: {
                    height: documentTemplate.headerHeight + 'mm',
                    contents: documentTemplate.header
                },
                "footer": {
                    "height":  documentTemplate.footerHeight + 'mm',
                    "contents": documentTemplate.footer
                }
            }

            let now = Date.now()
            let document = {
                html: documentTemplate.content,
                data: documentParams,
                path: `./public/agreement${now}.pdf`
            }

            pdf.create(document, options)
                .then(res => {
                    let pdf = fs.readFileSync(document.path);
                    fs.unlinkSync(document.path)
                    resolve(pdf)
                })
                .catch(error => {
                    console.error(error)
                    reject(error)
                })
        })
    }

}
