const ModuleArchitecture = require('../ModuleArchitecture')

module.exports = class userManagement extends ModuleArchitecture{
    constructor(ctx) {
        super(ctx)
    }

    createEmptySession (){
        return {
            username: "",
            access: [],
            activity: [],
            //if user is logged in then this flag is 1
            userAuth: false,
            userId: 0,
        }
    }

    userDescription(req) {
        if(req.session.user){
            return {
                username: req.session.user.username,
            }
        }else {
            return {
                name: "I don't know :p"
            }
        }
    }

    /**
     * Function verify user credentials and gives or not authorization
     * @param req
     */
    async authorize(req, username, password, effectiveUsername) {
        //set user authorize flag
        let c = this.getCtx().getCONST()

        let user = await this.getCtx().getQueries().userManagement.verifyUser({username, password})
        //console.log("user", user)
        //verify if default user (from const.json instead of database)
        if(c.defaultUser && user == null){
            //check for default user
            if(username == c.defaultUser.name && password == c.defaultUser.password){
                if(!req.session.user){
                    req.session.user = this.createEmptySession()
                }
                req.session.user.userAuth = true
            }
            //find in db
            else{
                //cannot find user
                return 1
            }
        }else if(user){
            if(!req.session.user){
                req.session.user = this.createEmptySession()
            }
            req.session.user.userAuth = true
            //console.log("logged in from db")
        }


        return 0
    }

    /**
     * Function removes authorization from user session
     * @param req
     */
    deauthorize(req) {
        console.log("try log out")
        if(req.session.user){
            req.session.user.username = ""
            req.session.user.userAuth = false
            console.log("log out", req.session.user)
        }

    }

    /**
     * Function used to check if user has authorization to use given endpoint
     * @param req
     * @param endpoint
     * @returns {boolean|*|boolean}
     */
    checkAuthorization(req, endpoint){
        if(req.session.user){
            return req.session.user.userAuth
        } else{
            return false
        }
    }

    middleware () {
        let self = this
        return function (req, res, next){
            //console.log("middleware v1", req.session.user)

            //console.log("req.cookies" , req.session)

            //check if user has been authorized
            if(req.session.user){
                let n = new Date()
                //req.session.user.activity.push(req.path)
                if(req.session.user.expire < n){
                    //logout user!
                    delete req.session.user
                }
            }
            //if not then create json object
            else{
                let expireDate = new Date()
                expireDate.setHours(expireDate.getHours()+1)
                req.session.user = self.createEmptySession()
            }

            next()
        }
    }
}
