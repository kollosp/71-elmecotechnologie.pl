const ModuleArchitecture = require("../ModuleArchitecture");
const nodemailer = require('nodemailer')

module.exports = class Mailer extends ModuleArchitecture {
    constructor(ctx) {
        super(ctx)
        if(ctx.getCONST().mailer){
            this.inited = true
            this.username = ctx.getCONST().mailer.username
            this.password =  ctx.getCONST().mailer.password


            this.transporter = nodemailer.createTransport({
                host: "s168.cyber-folks.pl", //
                port: 587,//465, //587,// ,
                secure: false, // use TLS
                auth:{
                    user: this.username,//, //username,
                    pass: this.password, //password, //""
                },
                tls: {
                    rejectUnauthorized: false
                }
            })


            console.log("Email tranporter created: ", this.username, this.password)
        }else{
            console.error("Mailer module stopped. Caused by incomplete config file.")
            this.inited = false
        }


        this._private = {}
    }

    middleware() {
        let self = this
        return function (req, res, next) {

        }
    }

    addToNotificationHistory(data){
        return this.getCtx().getQueries().mailManagement.insertHistory(data)
    }

    sendEmail(options){
        if(!this.inited) {
            console.error("Mailer module has not been initted correctly")
        }

        if(!options.bcc){
            options.bcc = "pparczyk@elmecosolar.pl"
        }

        let self = this
        return new Promise((resolve, reject) => {
            let mailOptions = {
                from: options.from || self.username, // sender address
                to: options.to, // list of receivers
                bcc: options.bcc,
                replyTo: options.replyTo || self.username,
                subject: options.subject || "Temat", // Subject line
                text: options.text || "Tresc", // plaintext body
                html: options.html // html body
            }

            self.transporter.sendMail(mailOptions, (error, data) => {
                if(error) {
                    self.addToNotificationHistory({
                        subject: options.subject,
                        content: options.text +  options.html,
                        receivers: options.to + "," + options.bcc,
                        status: false,
                        statusDescription: JSON.stringify(error),
                        date: new Date()
                    }).then((s)=>{
                        reject({
                            email: error,
                            emailHistory: s
                        })
                    })


                }
                else {
                    self.addToNotificationHistory({
                        subject: options.subject,
                        content: options.text +  options.html,
                        receivers: options.to + "," + options.bcc,
                        status: true,
                        statusDescription: JSON.stringify(data),
                        date: new Date()
                    }).then((s) => {
                        resolve({
                            email: data,
                            emailHistory: s
                        })
                    })

                }
            })
        })
    }

    sendNotification(fragmentId){
        if(!this.inited) {
            console.error("Mailer module has not been initted correctly")
        }

        return this.sendEmail({

        })
    }
}
