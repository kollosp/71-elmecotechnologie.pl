const ModuleArchitecture = require('../ModuleArchitecture')

module.exports = class CMS extends ModuleArchitecture {
    constructor(ctx) {
        super(ctx)

        this._private = {
            fragments: {},
            pages: {},
        }

        this.init().then()
    }

    /**
     * Load every possible fragments at the beginning. Disable this function in
     * constructor to enable lazy loading
     */
    async init() {
        let fragments = await this.getCtx().getQueries().fragmentManagement.findAllFragments()
        for(let i in fragments){
            this._private.fragments[fragments[i].key] = fragments[i].content
        }

        let pages = await this.getCtx().getQueries().pageManagement.findAllPages()
        for(let i in pages){
            this._private.pages[pages[i].url] = pages[i]
        }
    }

    /**
     * Load from database specified fragment
     * @param fragmentKey
     */
    async reloadFragment(fragmentKey){
        let fragment = await this.getCtx().getQueries().fragmentManagement.findFragment({key: fragmentKey})
        if(fragment == null) {
            this._private.fragments[fragmentKey] = ""
        } else {
            this._private.fragments[fragmentKey] = fragment
        }
    }

    async reloadPage(pageUrl){
        let page = await this.getCtx().getQueries().pageManagement.findPage({url: pageUrl})
        if(page == null) {
            this._private.pages[pageUrl] = null
        } else {
            this._private.pages[pageUrl] = page
        }
    }

    /**
     * Check if given fragment is already downloaded from db, if so then return on the other hands load from db
     * (when lazy loading enabled)
     * @param fragmentKey
     */
    async getFragment(fragmentKey){
        let lazy_loading = false
        if(lazy_loading && this._private.fragments[fragmentKey]){
            return this._private.fragments[fragmentKey]
        }else{
            await this.reloadFragment(fragmentKey)
            return this._private.fragments[fragmentKey]
        }
    }

    async getPage(pageUrl){
        let lazy_loading = false
        if(lazy_loading && this._private.pages[pageUrl]){
            return this._private.pages[pageUrl]
        }else{
            await this.reloadPage(pageUrl)
            return this._private.pages[pageUrl]
        }
    }

    /**
     * Function returns express middleware. The middleware should be placed as last middleware or the last one before 404
     * error handler. It catch the url and try to fit it with the urls defined in content management system.
     * @returns {function(...[*]=)}
     */
    middleware() {
        let self = this
        return async function(req, res, next){
            //normalize url. Remove slash if it is the last sing in url
            let url = decodeURI(req.url)
            if(url[url.length-1] == '/') url = url.slice(0,-1)

            let page = await self.getPage(url)

            if(!page){
                next()
                return
            }
            //console.log(url, "page requested. Responding withasdas", page)

            if(!page.pageType) {
                page.pageType = {name: "no type given"}
            }
            //pass session parameters to template engine
            res.locals.session = req.session
            res.locals.title = page.htmlTitle
            res.locals.description = page.htmlDescription
            res.locals.text = page.name

            //console.log(url, "page requested. Responding with", page)

            switch (page.pageType.name) {
                case "Autogenerated":
                    res.locals.content = "Strona generowana automatycznie"
                    res.locals.fragments = page.fragments
                    res.locals.tableOfContentsEnable = page.tableOfContentsEnable
                    res.render("blog")
                    break;
                case "HTMLFile":
                    res.locals.content = "Strona definiowana w pliku statycznym"
                    res.render(page.htmlTemplateFilePath)
                    break;
                case "HTMLTemplated":
                    res.locals.content = "Strona generowana na podstawie wzorca"
                    res.render("blog")
                    break;
                default: res.locals.content = "błąd tworzenia strony"
                    res.render("blog")
                    break;
            }
        }
    }
}
