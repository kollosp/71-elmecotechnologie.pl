module.exports = class Ctx {
    constructor() {
        this._private = {
            queries: {},
            modules: {},
            CONST: {},
            sequelizeDb: undefined,

        }
    }

    setSequelize (db){
        this._private.sequelizeDb = db
    }

    getSequelize(){
        if(this._private.sequelizeDb == undefined)
            throw Error("Sequelize object has not been set in context")

        return this._private.sequelizeDb
    }

    getOp(){
        if(this._private.sequelizeDb == undefined)
            throw Error("Sequelize object has not been set in context")

        return this._private.sequelizeDb.Sequelize.Op
    }

    getSequelizeInstance(){
        if(this._private.sequelizeDb == undefined)
            throw Error("Sequelize object has not been set in context")

        return this._private.sequelizeDb.Sequelize
    }

    setQueries(queries){
        this._private.queries = queries
    }

    setCONST(CONST) {
        this._private.CONST = Object.assign({}, CONST)
    }

    /**
     * Function allows to set available modules in the context object
     * @param modules
     */
    registerModules(modules){
        this._private.modules= modules
        this.printModules()
    }

    printModules () {
        //console.log("Modules loaded:")
        let index = 1
        for(let m in this._private.modules){
            console.log(`module ${index}. ${m}`)
            index ++
        }
    }

    /**
     * Function allow to access selected module by name
     * @param moduleName
     * @returns {null|ModuleArchitecture}
     */
    getModule(moduleName){
        for(let m in this._private.modules){
            if(m == moduleName){
                return this._private.modules[m]
            }
        }
        return null
    }

    getQueries() {
        return this._private.queries
    }

    getCONST() {
        return this._private.CONST
    }
}
