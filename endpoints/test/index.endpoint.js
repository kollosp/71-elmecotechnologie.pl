/**
 * File contains endpoints. Endpoint is a function which take req, res and next object and a name. Apart from function
 * endpoint contains some meta information regarding what privileges make user able to access to it.
 */

module.exports = [{
    name: '',
    method: 'get',
    function: async function(ctx, req, res) {
        res.send("Test index")
    }
},{
    name: 'hello',
    method: 'get',
    function: async function(ctx, req, res) {
        res.send("Test hello")
    }
},{
    name: 'bye',
    method: 'get',
    function: async function(ctx, req, res) {
        res.send("Test bye")
    }
}]
