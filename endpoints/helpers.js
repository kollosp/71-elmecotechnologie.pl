const helpers = {
    checkQuery: function (query, expectedQuery) {
        let config = query
        let keys = Object.keys(query)

        for (let i of expectedQuery) {
            if (keys.indexOf(i) > -1) {
                config[i] = query[i]
            } else {
                let exception = "required fields were not specified. url should be: "

                for (let j of expectedQuery) {
                    exception += '/' + j
                }

                exception += " given: " + JSON.stringify(query) + "." +
                    " not found field is: " + i

                throw(new Error(exception))
            }
        }

        return config
    },

    logError: function(error) {
        let d = new Date()
        if(error.stack){
            console.error(d.toLocaleTimeString() + " " + JSON.stringify(error.toString(), null,  4))
            console.error(error.stack, error.name, error.message)
        }else{
            console.error(d.toLocaleTimeString() + " " + error)
        }
    },

    basicMethodWrapper: function (expectedFields, method) {
        return async function (req, res) {
            try {
                let config = helpers.checkQuery(req.body, expectedFields)
                let data = await method(req, res, config)
                res.send(data)
            } catch (e) {
                helpers.logError(e)
                res.send({error: {stack: e.stack, message: e.message, name: e.name}})
            }
        }
    },

    checkedQueryMethodWrapper: function (expectedFields, queriesMethod) {
        return async function (req, res) {
            try {
                let config = helpers.checkQuery(req.body, expectedFields)
                let data = await queriesMethod(config)
                res.send(data)
            } catch (e) {
                console.log("checkedQueryMethodWrapper queriesMethod", queriesMethod)
                helpers.logError(e)
                res.send({error: {stack: e.stack, message: e.message, name: e.name}})
            }
        }
    }
}

module.exports = helpers
