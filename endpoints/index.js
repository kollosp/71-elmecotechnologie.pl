/**
 * index.js:
 * This is the main endpoints file. The file contains script which include/load every possible
 * endpoint. Endpoint is a controller in MVC template. The frontend get access to db or any other
 * part of the system via specific endpoint.
 *
 *
 * Structure and usage of *.endpoint.js:
 * Every file named by *.endpoint.js contains information about endpoints and middleware supported by the system. When
 * server is starting then every file in filesystem under the endpoints directory is interpreted. The interpretation
 * process is like:
 *  - go inside the given path.
 *  - find all files named with the following format pattern *.endpoint.js
 *  - create express router
 *  - add every endpoint and middleware to the router. (middleware is and endpoint where only function is defined)
 *  - for every directory in current directory execute this function recursively.
 *  - return build router
 *
 *  Example *endpoint.js file
 *  module.exports = [{                                     //exports array of objects
 *      name: '',                                           //name of the endpoint. Without '/' at the begin
 *      method: 'get',                                      //http method get, post, put, delete
 *      function: async function(ctx, req, res, next) {     //async function
 *          res.send("Hello world")
 *      }
 *  }]
 */

var express = require('express');

const fs = require('fs')
const path = require('path')
const helpers = require('./helpers')

const resolveFolder = function (currentPath, ctx) {
    //to do
    //add every index.endpoint.js file as a new router
    //in case of new directory go inside and then ...

    let router = express.Router();

    fs.readdirSync(currentPath).forEach(function(file) {
        //if endpoints
        if (file.match(/\.endpoint\.js$/) !== null) {
            let name = file.replace('.endpoint.js', '');
            if(name == 'index') name = ""
            let endpoints = require(path.join(currentPath, file))


            for(let endpoint of endpoints){
                //define middleware
                if(endpoint.name == undefined || endpoint.method == undefined){
                    router.use(function (req, res, next) {
                        endpoint.function(ctx, req, res, next, helpers)
                    })
                }
                //define endpoint
                else{
                    let methods = []
                    if(Array.isArray(endpoint.method)){
                        methods = endpoint.method
                    }else{
                        methods.push(endpoint.method)
                    }

                    for(let method of  methods) {
                        switch (method) {
                            case 'get':
                                router.get('/' + endpoint.name, function (req, res, next) {
                                    endpoint.function(ctx, req, res, helpers)
                                })
                                break;
                            case 'post':
                                router.post('/' + endpoint.name, function (req, res, next) {
                                    endpoint.function(ctx, req, res, helpers)
                                })
                                break;
                            case 'delete':
                                router.delete('/' + endpoint.name, function (req, res, next) {
                                    endpoint.function(ctx, req, res, helpers)
                                })
                                break;
                            case 'put':
                                router.put('/' + endpoint.name, function (req, res, next) {
                                    endpoint.function(ctx, req, res, helpers)
                                })
                                break;
                        }
                    }
                }
            }
        }
        //if folder
        else if(fs.lstatSync(path.join(currentPath, file)).isDirectory()){
            router.use('/' + file, resolveFolder(path.join(currentPath, file),ctx))
        }
    })

    return router
}




module.exports = (ctx) => {
    return resolveFolder(__dirname,ctx);
}
