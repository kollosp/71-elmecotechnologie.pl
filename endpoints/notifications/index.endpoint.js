module.exports = [{
    name: 'generatenotificationlist',
    method: 'post',
    function: async function(ctx, req, res, helpers) {
        let ret = await ctx.getModule("notifications").generateNotificationsList({
            date: req.body.date
        })
        res.send(ret)
    }
},{
    name: 'notify',
    method: 'post',
    function: async function(ctx, req, res, helpers) {
        await ctx.getModule("notifications").notify({
            heatpumpId: req.body.heatpumpId,
            notificationTypeId: req.body.notificationTypeId,
            date: req.body.date
        })
        res.sendStatus(200)
    }
}]
