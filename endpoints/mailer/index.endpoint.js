module.exports = [{
    name: 'sendemail',
    method: 'post',
    function: async function(ctx, req, res, helpers) {

        await ctx.getModule("mailer").sendEmail({
            replyTo: req.body.email,
            subject: req.body.subject,
            html:
`Email wygenerowany przez system elmecotechnologie.pl.<br>
Wiadomość od: ${req.body.name} ${req.body.surname}<br>
Email: ${req.body.email}<br>
Telefon: ${req.body.phone}<br>
<br><br>
Przeglądana strona: <a href="${req.body.url}">${req.body.url}</a><br>
Temat: ${req.body.subject}<br>
Treść:</br>
${req.body.message}`,
            to:"biuro@elmecosolar.pl"
        })
        res.sendStatus(200)
    }
},{
    name: "sendnotification",
    method: 'post',
    function: async function(ctx,req,res,helpers){
        console.log(req.body)
        res.sendStatus(200)
    }
}]
