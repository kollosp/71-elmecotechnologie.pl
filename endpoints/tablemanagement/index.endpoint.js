module.exports = [{
    function: async function(ctx, req, res, next) {
        if(ctx.getModule('userManagement').checkAuthorization(req, req.path)){
            next()
        }else{
            res.status(401).send("access denied")
        }
    }
},{
    name: 'showtables',
    method: 'post',
    function: async function(ctx, req, res, helpers) {
        await helpers.checkedQueryMethodWrapper(
            [],
            ctx.getQueries().tableManagement.showTables
        )(req, res)
    }
},{
    name: 'describetable',
    method: 'post',
    function: async function(ctx, req, res, helpers) {
        await helpers.basicMethodWrapper(
        ['table'],
        async function (req, res, config) {
            return ctx.getQueries().tableManagement.describeTable(config.table);
        })(req, res)
    }
},{
    name: 'findAll',
    method: 'post',
    function: async function(ctx, req, res, helpers) {
        await helpers.checkedQueryMethodWrapper(
            ['table', 'constraints'],
            ctx.getQueries().tableManagement.findAll
        )(req, res)
    }
},{
    name: 'findAllAssociated',
    method: 'post',
    function: async function(ctx, req, res, helpers) {
        await helpers.checkedQueryMethodWrapper(
            ['table', 'associated', 'as'],
            ctx.getQueries().tableManagement.findAllAssociated
        )(req, res)
    }
},{
    name: 'count',
    method: 'post',
    function: async function(ctx, req, res, helpers) {
        await helpers.checkedQueryMethodWrapper(
            ['table', 'constraints'],
            ctx.getQueries().tableManagement.countTable
        )(req, res)
    }
},{
    name: 'findOne',
    method: 'post',
    function: async function(ctx, req, res, helpers) {
        await helpers.checkedQueryMethodWrapper(
            ['table', 'id'],
            ctx.getQueries().tableManagement.findOne
        )(req, res)
    }
},{
    name: 'updateorinsert',
    method: 'post',
    function: async function(ctx, req, res, helpers) {
        await helpers.checkedQueryMethodWrapper(
            ['table', 'id', 'data'],
            ctx.getQueries().tableManagement.updateOrInsert
        )(req, res)
    }
},{
    name: 'remove',
    method: 'post',
    function: async function(ctx, req, res, helpers) {
        await helpers.checkedQueryMethodWrapper(
            ['table', 'id'],
            ctx.getQueries().tableManagement.destroyRow
        )(req, res)
    }
},{
    name: 'name',
    method: 'post',
    function: async function(ctx, req, res, helpers) {
        await helpers.checkedQueryMethodWrapper(
            ['table', 'id'],
            ctx.getQueries().tableManagement.name
        )(req, res)
    }
},{
    name: 'description',
    method: 'post',
    function: async function(ctx, req, res, helpers) {
        await helpers.checkedQueryMethodWrapper(
            ['table', 'id'],
            ctx.getQueries().tableManagement.description
        )(req, res)
    }
}]
