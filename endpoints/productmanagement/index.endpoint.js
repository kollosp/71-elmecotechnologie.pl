module.exports = [{
    function: async function(ctx, req, res, next) {
        console.log("check if user is logged in")
        next()
    }
},{
    name: 'generateqrcode',
    method: 'post',
    function: async function(ctx, req, res, helpers) {
        await helpers.checkedQueryMethodWrapper(
            ['productId'],
            ctx.getQueries().productManagement.generateQrCode
        )(req, res)
    }
}]
