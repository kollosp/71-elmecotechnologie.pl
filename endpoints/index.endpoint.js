const path = require('path')

module.exports = [{
    function: async function(ctx, req, res, next) {
        next()
    }
},{
    name: '',
    method: 'get',
    function: async function(ctx, req, res) {
        res.render('index', {session: req.session, title: "Elmeco Technologie - Montaż, doradztwo, dystrybucja najwyższej jakości ", description:"Elmeco Technologie", content: await ctx.getModule("cms").getFragment("MainPage")})
    }
},{
    name: 'katalog*',
    method: 'get',
    function: async function(ctx, req, res) {

        let urlElements = decodeURI(req.path).split('/')

        urlElements = urlElements.map(element => { return decodeURIComponent(element)})

        //console.log("urlElements", urlElements)
        //remove endpoint name
        urlElements.shift()

        //no elements given show main page
        if(urlElements.length == 0){

            res.render('catalog', {session: req.session, title: "Katalog - brak produktu", description: "Niestety nie udało sie odnaleść poszukiwanego elementu na storonie", url: req.url, breadcrumb: []})

        }
        //some type or product specified
        else{

            //remove last element if url is ending with '/'
            if(urlElements[urlElements.length-1] == "")
                urlElements.pop()

            let last = urlElements[urlElements.length-1]

            let product = await ctx.getModule("productManagement").loadProduct(last)

            //display product page
            if(product){

                res.render('product', {session: req.session, title: product.description, description: product.description, url: req.url, breadcrumb: urlElements, product})
            }
            //load categories and products
            else{
                let categories = []
                let products = []
                if(urlElements.length == 1){
                    categories = await ctx.getModule("productManagement").loadSubtypesOf(null)
                }else{
                    categories = await ctx.getModule("productManagement").loadSubtypesOf(last)
                    console.log("last categories" , last)
                    products = await ctx.getModule("productManagement").loadProductsOfType(last)
                    products.forEach((el, index, array) => {
                        array[index].path = path.join(req.url, encodeURIComponent(el.name))
                    })
                }

                //console.log(categories)

                //add paths to objects
                categories.forEach((el, index, array) => {
                    array[index].path = path.join(req.url, encodeURIComponent(el.name))
                })

                res.render('catalog', {session: req.session, title: `Elmecotechnologie | ${last}`, description: last, url: req.url, breadcrumb: urlElements, categories, products})
            }
        }
    }
},

//section protected by user authorization
{
    name: 'dashboard',
    method: 'get',
    function: async function(ctx, req, res) {

        if(ctx.getModule('userManagement').checkAuthorization(req, req.path)){
            res.render('dashboard', {title: "Elmeco Technologie - Panel administracyjny", description: ""})
        }else{
            res.status(401).send("access denied")
        }

    }
}]
