module.exports = [{
    function: async function(ctx, req, res, next) {
        next()
    }
},{
    name: 'login',
    method: 'post',
    function: async function(ctx, req, res) {
        //perform login operation....
        let auth = await ctx.getModule("userManagement").authorize(req,req.body.username, req.body.password)

        //authorization correct
        if(auth == 0){
            req.session.user.username = req.body.username
            req.session.save(() => {
                res.redirect('/dashboard')
            })
        }
        //authorization fault
        else{
            res.status(401).send("Credentials wrong")
        }
    }
},{
    name: 'verify',
    method: 'post',
    function: async function(ctx, req, res) {
        //perform login operation....

        return ({token: 123456789})
    }
}, {
    name: 'whoami',
    method: 'post',
    function: async function(ctx, req, res) {
        res.send(ctx.getModule("userManagement").userDescription(req))
    }
}, {
    name: 'logout',
    method: 'get',
    function: async function (ctx, req, res) {

        ctx.getModule("userManagement").deauthorize(req)
        req.session.save(() => {
            res.redirect('/login')
        })

    }
}]
