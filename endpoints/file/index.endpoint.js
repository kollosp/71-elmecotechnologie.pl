module.exports = [{
    name: '',
    method: 'get',
    function: async function(ctx, req, res) {
        let files = await ctx.getQueries().fileManagement.findFiles()

        //build list of available files
        let txt = files.reduce((acc, value) => {
            return acc + `<li><a href='/file/${value.name}'>${value.name} ${value.extension}</a></li>`
        }, "")

        res.status(200).send(`<ol>${txt}</ol>`);
    }
},{
    name: ':filename',
    method: 'get',
    function: async function(ctx, req, res) {
        let file = await ctx.getQueries().fileManagement.findFile({filename: req.params.filename})

        if(!file){
            res.status(404).send('Not found');
        }else{
            res.contentType(file.extension || 'image/jpeg');
            res.end(file.data, 'binary');
        }
    }
}]
