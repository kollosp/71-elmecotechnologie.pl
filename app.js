var createError = require('http-errors');
var express = require('express');
var session = require('express-session');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require("body-parser");
var FileStore = require('session-file-store')(session);

//var indexRouter = require('./routes/index');
//var usersRouter = require('./routes/users');

var app = express();

var sess = {
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  store: new FileStore({path: '/tmp/sessionStore'}),
  cookie: {}
}

//create context object
//context is used to store and share application state and functionalities
const Ctx = require('./lib/Ctx')
const ctx = new Ctx()

const CONST = require("./lib/constModule")(__dirname)

process.env.NODE_ENV = CONST.NODE_ENV || "production"
//process.logger.info(`starting in ${process.env.NODE_ENV} mode`)

ctx.setCONST(CONST)
ctx.setSequelize(require('./models'))

ctx.setQueries(require('./queries')(ctx))
ctx.registerModules(require('./modules')(ctx))

// view engine setup
app.set('views', path.join(__dirname, 'public'));
app.set('view engine', 'ejs');

if (app.get('env') === 'production') {
  app.set('trust proxy', 1) // trust first proxy
  sess.cookie.secure = true // serve secure cookies
}
//setup request processing
app.use(bodyParser.json({limit: '3000kb'}));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

//process request
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'static')));

app.use(session(sess))
app.use(ctx.getModule("userManagement").middleware());

app.use('/', require('./endpoints')(ctx))

//console.log(ctx.getQueries())
app.use(function(req, res, next) {
  next()
});

//pages from db
app.use(ctx.getModule("cms").middleware());
app.use(function(req, res, next) {
  next()
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  console.log("error 404")
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.session = req.session
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error', {title: err.status, description:"Wystapił nieoczekiwany błąd", message: err.message, error: err});
});

module.exports = app;
