'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('PM_IMAGE', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      data: {
        type: Sequelize.BLOB('long')
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING(100),
        unique: true
      },
      alt: {
        allowNull: false,
        defaultValue: "",
        type: Sequelize.STRING
      },
      extension: {
        allowNull: false,
        defaultValue: "",
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('PM_IMAGE');
  }
};
