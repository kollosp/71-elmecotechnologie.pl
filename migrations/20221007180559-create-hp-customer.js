'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('HP_CUSTOMER', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ""
      },
      surname: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ""
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ""
      },
      altEmail: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ""
      },
      phone: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ""
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('HP_CUSTOMER');
  }
};
