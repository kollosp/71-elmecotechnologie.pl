'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('HP_HEATPUMPFILE', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      heatpumpId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'HP_HEATPUMP',
          key: 'id'
        },
        allowNull: false
      },
      fileId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'PM_IMAGE',
          key: 'id'
        },
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('HP_HEATPUMPFILE');
  }
};
