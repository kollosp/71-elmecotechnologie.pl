'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('PM_PRODUCT', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      description: {
        type: Sequelize.STRING
      },
      shortDesc: {
        type: Sequelize.STRING
      },
      model: {
        type: Sequelize.STRING
      },
      model_2: {
        type: Sequelize.STRING
      },
      manufacturerId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'PM_MANUFACTURER',
          key: 'id'
        }
      },
      price: {
        type: Sequelize.FLOAT
      },
      available: {
        type: Sequelize.INTEGER
      },
      jsonParams: {
        type: Sequelize.STRING
      },
      productTypeId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'PM_PRODUCTTYPE',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('PM_PRODUCT');
  }
};
