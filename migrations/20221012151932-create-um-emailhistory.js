'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('UM_EMAILHISTORY', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      date: {
        type: Sequelize.DATE
      },
      subject: {
        type: Sequelize.STRING
      },
      receivers: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.INTEGER
      },
      content: {
        type: Sequelize.TEXT
      },
      statusDescription: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('UM_EMAILHISTORY');
  }
};
