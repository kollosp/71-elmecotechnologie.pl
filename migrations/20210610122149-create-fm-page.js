'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FM_PAGE', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING(191),
        allowNull: false,
        unique: true,
      },
      htmlTitle: {
        type: Sequelize.STRING(75),
        allowNull: false,
        unique: true,
      },
      htmlDescription: {
        type: Sequelize.STRING(130),
        allowNull: false,
        defaultValue: "",
      },
      htmlTags: {
        type: Sequelize.STRING,
        defaultValue: "",
        allowNull: false,
      },
      url: {
        type: Sequelize.STRING(191),
        allowNull: false,
        unique: true,
      },
      available: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      pageTypeId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'FM_PAGETYPE',
          key: 'id'
        }
      },
      htmlTemplate: {
        type: Sequelize.TEXT,
        defaultValue: "",
        allowNull: false,
      },
      htmlTemplateFilePath: {
        type: Sequelize.STRING,
        defaultValue: "",
        allowNull: false,
      },
      predictEnable: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      tableOfContentsEnable: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('FM_PAGE');
  }
};
