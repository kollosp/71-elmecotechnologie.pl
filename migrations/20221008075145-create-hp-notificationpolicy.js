'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('HP_NOTIFICATIONPOLICY', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      period: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 12
      },
      firstNotification: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 30
      },
      secondNotification: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 15
      },
      thirdNotification: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 7
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('HP_NOTIFICATIONPOLICY');
  }
};
