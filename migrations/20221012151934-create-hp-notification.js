'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('HP_NOTIFICATION', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      date: {
        type: Sequelize.DATE
      },
      heatpumpId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'HP_HEATPUMP',
          key: 'id'
        }
      },
      emailhistoryId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'UM_EMAILHISTORY',
          key: 'id'
        }
      },
      type:{
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('HP_NOTIFICATION');
  }
};
