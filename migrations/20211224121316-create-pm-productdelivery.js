'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('PM_PRODUCTDELIVERY', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      productId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'PM_PRODUCT',
          key: 'id'
        }
      },
      deliveryId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'PM_DELIVERY',
          key: 'id'
        }
      },
      productCount: {
        allowNull: false,
        type: Sequelize.FLOAT,
        defaultValue: 0
      },
      productPrice: {
        allowNull: false,
        type: Sequelize.FLOAT,
        defaultValue: 0
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('PM_PRODUCTDELIVERY');
  }
};
