'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('PM_IMAGE', 'isAutoCreated', {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      }),
    ])
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('PM_PRODUCT', 'isAutoCreated'),
    ])
  }
};
