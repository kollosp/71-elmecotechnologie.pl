'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('PM_DELIVERY', 'warehouseId', {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'PM_WAREHOUSE',
          key: 'id'
        }
      }),
    ])
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('PM_DELIVERY', 'warehouseId'),
    ])
  }
};
