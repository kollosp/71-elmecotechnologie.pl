'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('PM_PRODUCTTYPEIMAGE', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      productTypeId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'PM_PRODUCTTYPE',
          key: 'id'
        }
      },
      imageId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'PM_IMAGE',
          key: 'id'
        }
      },
      index: {
        allowNull: false,
        defaultValue: 0,
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('PM_PRODUCTTYPEIMAGE');
  }
};
