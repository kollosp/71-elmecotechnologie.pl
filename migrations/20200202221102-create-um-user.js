'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('UM_USER', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      loginDataId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'UM_LOGINDATA',
          key: 'id'
        }
      },
      personDataId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'UM_PERSONDATA',
          key: 'id'
        }
      },
      emailId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'UM_EMAIL',
          key: 'id'
        }
      },
      deleted: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('UM_USER');
  }
};