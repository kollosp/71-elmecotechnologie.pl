'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('HP_HEATPUMP', 'notificationPolicyId', {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'HP_NOTIFICATIONPOLICY',
          key: 'id'
        },
        defaultValue: 1,
      }),
    ])
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('HP_HEATPUMP', 'notificationPolicyId'),
    ])
  }
};
