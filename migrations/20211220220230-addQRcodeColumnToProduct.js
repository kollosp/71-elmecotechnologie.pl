'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('PM_PRODUCT', 'qrImageId', {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'PM_IMAGE',
          key: 'id'
        }
      }),
    ])
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('PM_PRODUCT', 'qrImageId'),
    ])
  }
};
