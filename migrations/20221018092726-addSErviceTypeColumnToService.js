'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('HP_HEATPUMPSERVICE', 'serviceTypeId', {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'HP_HEATPUMPSERVICETYPE',
          key: 'id'
        }
      }),
    ])
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('HP_HEATPUMPSERVICE', 'serviceTypeId'),
    ])
  }
};
