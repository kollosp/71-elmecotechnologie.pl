'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('PM_DELIVERY', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "",
      },
      note: {
        allowNull: false,
        type: Sequelize.TEXT,
        defaultValue: ""
      },
      date: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deliveryTypeId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'PM_DELIVERYTYPE',
          key: 'id'
        }
      },
      deliveryDirection: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: 0,
        comment: "0 delivery, 1 picked up"
      },
      userId: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'UM_USER',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('PM_DELIVERY');
  }
};
