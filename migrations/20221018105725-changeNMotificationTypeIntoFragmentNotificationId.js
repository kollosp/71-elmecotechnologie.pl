'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('HP_NOTIFICATION', 'type'),
      queryInterface.addColumn('HP_NOTIFICATION', 'fragmentNotificationId', {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'HP_FRAGMENTNOTIFICATION',
          key: 'id'
        },
        defaultValue: 1,
      }),
    ])
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('HP_NOTIFICATION', 'fragmentNotificationId'),
      queryInterface.addColumn('HP_NOTIFICATION', 'type', {
        type: Sequelize.STRING,
      }),
    ])
  }
};
