'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.changeColumn('HP_HEATPUMP', 'refrigerantWeight', {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      }),
    ])
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.changeColumn('HP_HEATPUMP', 'refrigerantWeight', {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      }),
    ])
  }
};
