'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('HP_HEATPUMPSERVICEFILE', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      heatpumpserviceId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'HP_HEATPUMPSERVICE',
          key: 'id'
        },
        allowNull: false
      },
      fileId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'PM_IMAGE',
          key: 'id'
        },
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('HP_HEATPUMPSERVICEFILE');
  }
};
