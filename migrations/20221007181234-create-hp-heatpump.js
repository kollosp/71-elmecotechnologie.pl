'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('HP_HEATPUMP', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      productId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'PM_PRODUCT',
          key: 'id'
        },
        allowNull: false
      },
      customerId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'HP_CUSTOMER',
          key: 'id'
        },
      },
      mountedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      notes: {
        type: Sequelize.TEXT,
        allowNull: false,
        defaultValue: ""
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ""
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ""
      },
      code: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ""
      },
      street: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ""
      },
      refrigerantId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'HP_REFRIGERANT',
          key: 'id'
        },
        allowNull: false
      },
      refrigerantWeight: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      customerNotificationAllowed: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      serviceNotificationAllowed: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('HP_HEATPUMP');
  }
};
